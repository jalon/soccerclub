<?php

namespace AppClass\Seg;


/**
 * Usuário
 *
 * @package Usuario
 * @author Jalon V�tor Cerqueira Silva
 * @version 1.0.1
 */
class Usuario {

	/**
	 * Usuario
	 * @var unknown
	 */
	private $_usuario;
	
	/**
	 * Código Usuario postado no formulário
	 * @var unknown
	 */
	private $_codUsuario;
	
	/**
	 * Associação Usuário - Organizacao
	 * @var unknown
	 */
	private $_oUsuOrg;
	
	/**
	 * Indicador Endereço obrigatório
	 * @var unknown
	 */
	
	private $_indEndObrigatorio;
	
	/**
	 * Perfil
	 * @var unknown
	 */
	private $_perfil;
	
	/**
	 * Cod Organizacao
	 * @var unknown
	 */
	private $_codOrganizacao;
	
	/**
	 * Confirmação de envio de email
	 * @var unknown
	 */
	private $_enviarEmail;
	
	/**
	 * Ententidade do telefone
	 * @var unknown
	 */
	private $_entidadeTel;
	
	/**
	 * Telefones
	 * @var unknown
	 */
	private $_telefone;
	
	/**
	 * Codigo do tipo de telefone
	 * @var unknown
	 */
	private $_codTipoTel;
	
	/**
	 * Codigo do telefone
	 * @var unknown
	 */
	private $_codTelefone;
	
	/**
     * Construtor
     *
	 * @return void
	 */
	public function __construct() {
		global $log;
		$log->debug(__CLASS__.": nova instância");
		
	}
	
	public function _setCodUsuario($codigo) {
		$this->_codUsuario = $codigo;
	}
	
	public function _getCodUsuario() {
		return ($this->_codUsuario);
	}
	
	public function _setUsuOrg($usuOrg) {
		$this->_oUsuOrg = $usuOrg;
	}
	
	public function _getUsuOrg() {
		return ($this->_oUsuOrg);
	}
	
	public function _setIndEndObrigatorio($valor) {
		$this->_indEndObrigatorio = $valor;
	}
	
	public function _getIndEndObrigatorio() {
		return ($this->_indEndObrigatorio);
	}
	
	public function _setPerfil($codigo) {
		$this->_perfil = $codigo;
	}
	
	public function _getPerfil() {
		return ($this->_perfil);
	}
	
	public function _setCodOrganizacao($codigo) {
		$this->_codOrganizacao = $codigo;
	}
	
	public function _getCodOrganizacao() {
		return ($this->_codOrganizacao);
	}
	
	//GET-SET ENTIDADE TELEFONE
	public function _setEntidadeTel($entidade) {
		$this->_entidadeTel = $entidade;
	}
	
	public function _getEntidadeTel() {
		return ($this->_entidadeTel);
	}
	
	public function _setTelefone(array $telefone) {
		$this->_telefone = $telefone;
	}
	
	public function _getTelefone() {
		return ($this->_telefone);
	}
	
	public function _setCodTipoTel(array $tipoTelefone) {
		$this->_codTipoTel = $tipoTelefone;
	}
	
	public function _getCodTipoTel() {
		return ($this->_codTipoTel);
	}
	
	public function _setCodTelefone(array $codTelefone) {
		$this->_codTelefone = $codTelefone;
	}
	
	public function _getCodTelefone() {
		return ($this->_codTelefone);
	}
	
	public function _setEnviarEmail($enviarEmail) {
		$this->_enviarEmail = $enviarEmail;
	}
	
	public function _getEnviarEmail() {
		return ($this->_enviarEmail);
	}
	
    /**
     * Lista os menus do usuário em uma determinada empresa
     */
    public static function listaMenusAcesso ($codUsuario) {
    	global $db,$log,$system;
    	try {
    		$info = $db->extraiTodos('SELECT M.CODIGO AS CODIGO, M.COD_PAI AS COD_PAI, M.NOME AS NOME, M.DESCRICAO AS DESCRICAO, M.COD_TIPO, M.LINK, M.ICONE, MP.ORDEM, MP.COD_TIPO_PERFIL, U.USUARIO
				FROM `SLAPP_MENU` M
					LEFT OUTER JOIN `SLAPP_MENU_PERFIL` MP ON (M.CODIGO = MP.COD_MENU) 
			        LEFT OUTER JOIN `SLADM_PERFIL_ACESSO` A ON (MP.COD_TIPO_PERFIL = A.CODIGO) 
			        LEFT OUTER JOIN `SLSEG_USUARIO` U ON (A.CODIGO = U.COD_PERFIL)
			        	WHERE U.CODIGO = :codUsuario
    						ORDER BY MP.ORDEM ASC', 
    				array(':codUsuario' => $codUsuario ));
	    	return($info);
	    }catch (\Doctrine\ORM\ORMException $e) {
	    	\AppClass\App\Erro::halt($e->getMessage());
	    }
    	
    }
    
    /**
     * Lista os menus do usuário em uma determinada empresa
     */
    public static function listaGrupoAcesso () {
    	global $db,$log,$system;
    	try {
    		$info = $db->extraiTodos('SELECT GA.CODIGO AS CODIGO, GA.DESCRICAO AS DESCRICAO 
				FROM `SLAPP_MENU_PERFIL` MP
					LEFT OUTER JOIN `SLADM_GRUPO_ACESSO` GA ON (MP.COD_GRUPO_ACESSO = GA.CODIGO)
			        LEFT OUTER JOIN `SLADM_PERFIL_ACESSO` A ON (MP.COD_TIPO_PERFIL = A.CODIGO)
			        LEFT OUTER JOIN `SLSEG_USUARIO` U ON (A.CODIGO = U.COD_PERFIL)
			        	WHERE U.CODIGO = :codUsuario
    						GROUP BY CODIGO',
    				array(':codUsuario' => $system->getCodUsuario()
    				));
    		return($info);
    	}catch (\Doctrine\ORM\ORMException $e) {
    		\AppClass\App\Erro::halt($e->getMessage());
    	}
    	 
    }
    
    /**
     * Verifica se o email de usuário já está cadastrado
     */

    public static function verificaEmail($email){
    	global $db,$log,$system;
    	
    	try {
    		$info	 = $db->extraiPrimeiro('SELECT * FROM `SLSEG_USUARIO` WHERE USUARIO = :usuario',
    				array(':usuario' => $email ));
        		
    		if(!$info){
    			return false;
    		}elseif(!isset($info->USUARIO) || empty($info->USUARIO)) {
    			return false;
    		}else {
    			return true;
    		}
    	} catch ( \Exception $e ) {
    		return false;
    	}
    }
    
	/**
	 * Busca usuários
	 */
	public static function busca ($sBusca = null,$start = 0,$limite = 10, $colunaOrdem = null,$dirOrdem = null) {
		global $em,$tr,$system;
		 
		//$em->getRepository('Entidades\ZgsegUsuario')->findAll();
		$qb 	= $em->createQueryBuilder();
		
		try {
			$qb->select('u')
			->from('\Entidades\ZgsegUsuario','u')
	    	->leftJoin('\Entidades\ZgsegUsuarioStatusTipo'	,'st'	, \Doctrine\ORM\Query\Expr\Join::WITH, 'u.codStatus = st.codigo')
			->where($qb->expr()->eq('u.codOrganizacao'	, ':codOrg'))
			->setParameter('codOrg', $system->getCodOrganizacao());
			
			if ($colunaOrdem !== null) {
				$dir	= strtoupper($dirOrdem);
				if (!$dir)	$dir = "ASC";
				$qb->orderBy("u.".$colunaOrdem, $dir);
			}
			
/*			->orderBy('u.nome', 'ASC')
*/
			if ($sBusca) {
				$qb->andWhere($qb->expr()->orx(
					$qb->expr()->like($qb->expr()->upper('u.usuario'), ':busca'),
					$qb->expr()->like($qb->expr()->upper('u.nome'), ':busca'),
					$qb->expr()->like($qb->expr()->upper('u.email'), ':busca')
				))
				->setParameter('busca', '%'.strtoupper($sBusca).'%');
			}
				
			
			if ($start 	!== null) $qb->setFirstResult( $start );
			if ($limite	!== null) $qb->setMaxResults( $limite );
			 
		
			$query = $qb->getQuery();
			return ($query->getResult());
		
		}catch (\Doctrine\ORM\ORMException $e) {
			\AppClass\App\Erro::halt($e->getMessage());
		}
	
	}	
	
	public function _getCodigo() {
		return $this->_usuario->getCodigo();
	}
	
	public function _getUsuario() {
		return $this->_usuario;
	}
	
}
