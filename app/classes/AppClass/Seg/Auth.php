<?php

namespace AppClass\Seg;

/**
 * Gerenciar a autenticação
 *
 * @package \AppClass\Seg\Auth
 * @author Jalon V�tor Cerqueira Silva
 * @version 1.0.1
 * @created 22/11/2015
 */
class Auth implements \Zend\Authentication\Adapter\AdapterInterface {

	/**
	 * Usuário
	 */
	private $username;
	
	/**
	 * Senha
	 */
	private $password;
	
	/**
	 * Sets username and password for authentication
	 *
	 * @return void
	 */
	public function __construct($username,$password) {
	
		/** Definindo Variáveis globais **/
		global $log;
	
		$log->debug(__CLASS__.": nova instância");
		$log->debug(__CLASS__.': Definindo usuario: '.$username);
	
		$this->username 		= $username;
		$this->password			= $password;
	}
	
	/**
	 * Faz a autenticação
	 *
	 * @throws Zend_Auth_Adapter_Exception If authentication cannot be performed
	 * @return Zend_Auth_Result
	 */
	public function authenticate() {
		global $db,$log,$system;
		

		/** Verifica se o usuário existe e resgata os dados dele **/
		$user = $db->extraiPrimeiro('SELECT * FROM `SLSEG_USUARIO` WHERE USUARIO = :usuario', array(':usuario' => $this->username ));
		
		if ($user) {
			/** Verifica se a senha está correta **/
			if (isset($user->SENHA) && $user->SENHA !== $this->password) {
				$log->debug('senha incorreta');
				$result		= \Zend\Authentication\Result::FAILURE_CREDENTIAL_INVALID;
				$messages[] = "Informações incorretas !!!";
				return new \Zend\Authentication\Result($result,$this->username,$messages);
			}
			
			/** Verifica se o usuário está ativo **/
			/* DEPOIS
			 * if ($user->getCodStatus()->getIndPermiteAcesso() == 0) {
				$result		= \Zend\Authentication\Result::FAILURE_CREDENTIAL_INVALID;
				$messages[] = "Usuário bloqueado/desativado !!!";
				return new \Zend\Authentication\Result($result,$this->username,$messages);
			}*/
			
			$result	= \Zend\Authentication\Result::SUCCESS;
			$messages[] = null;
			return new \Zend\Authentication\Result($result,$this->username,$messages);
		}else{
			$result		= \Zend\Authentication\Result::FAILURE_CREDENTIAL_INVALID;
			$messages[] = "Informações incorretas !!!";
			return new \Zend\Authentication\Result($result,$this->username,$messages);
		}
	}
}
