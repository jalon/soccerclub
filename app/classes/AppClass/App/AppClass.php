<?php
namespace AppClass\App;

/**
 * Implementação do DBApp
 * 
 * @package: System
 * @Author: Jalon Vitor Cerqueira Silva
 * @version: 1.0.1
 * 
 */
class AppClass extends \AppClass\App\ZWS {
	
	/**
	 * Objeto que irá guardar a Instância para implementar SINGLETON (http://www.php.net/manual/pt_BR/language.oop5.patterns.php)
	 */
	private static $instance;
	
	/**
	 * Código da empresa selecionada
	 *
	 * @var int
	 */
	private $codEmpresa;
	
	private $codGrupoAcesso;

	/**
	 * Código da Matriz
	 *
	 * @var int
	 */
	private $codMatriz;
	
	/**
	 * Código da linguagem padrão (Internacionalização)
	 *
	 * @var int
	 */
	private $codLang;
	
	/**
	 * Código da Organização
	 *
	 * @var int
	 */
	private $codOrganizacao;
	
	/**
	 * Configurações do Dynamic Html Load
	 */
	private $dynHtmlLoad;
	
	/**
	 * Url do site inicial
	 * @var string
	 */
	private $homeUrl;
	
	/**
	 * Nome do iframe central
	 * @var string
	 */
	private $divCentral;
	
	/**
	 * Construtor
	 *
	 * @return void
	 */
	protected function __construct() {
	/**
	 * Verificar função inicializaSistema() *
	 */
	}
	
	/**
	 * Construtor para implemetar SINGLETON
	 *
	 * @return object
	 */
	public static function getInstance() {
		if (! isset ( self::$instance )) {
			$c = __CLASS__;
			self::$instance = new $c ();
		}
		
		return self::$instance;
	}
	
	/**
	 * Refazer a função para não permitir a clonagem deste objeto.
	 */
	public function __clone() {
		\AppClass\App\Erro::halt ( 'Não é permitido clonar ' );
	}
	
    
	/**
	 * Inicializar o sistema
	 * @return void
	 */
    public function inicializaSistema () {
    	global $log,$db;
    	
    	/** Chama o construtor da classe mae **/
		parent::__construct();	
    	
		$log->debug(__CLASS__.": nova Instância");

		/** Definindo atributos globais a Instância de e-mail (Podem ser alterados no momento do envio do e-mail) **/
		//$this->mail->setSubject('.:: Erro no sistema ::.');
    }

    /**
     * Definir o Html Load
     *
     * @param string $valor
     */
    public function setDynHtmlLoad($valor) {
    	$this->dynHtmlLoad	= $valor;
    }
    
    /**
     * Resgatar o Html Load
     *
     * @return string
     */
    public function getDynHtmlLoad() {
    	return ($this->dynHtmlLoad);
    }
    
	/**
	 * @return the $homeUrl
	 */
	public function getHomeUrl() {
		return $this->homeUrl;
	}

	/**
	 * @param string $homeUrl
	 */
	public function setHomeUrl($homeUrl) {
		$this->homeUrl = $homeUrl;
	}

	/**
	 * @return the $divCentral
	 */
	public function getDivCentral() {
		return $this->divCentral;
	}

	/**
	 * @param string $divCentral
	 */
	public function setDivCentral($divCentral) {
		$this->divCentral = $divCentral;
	}
    
    /**
     * Gerar o html da Combo
     *
     * @return string
     */
	public function geraHtmlCombo($array,$codigo,$valor,$codigoSel = null,$valorDefault = null) {
		global $system;
	
		$accessor = \Symfony\Component\PropertyAccess\PropertyAccess::createPropertyAccessor();
	
		$html   = '';
		if ($valorDefault !== null) {
			($codigoSel == null) ? $selected = "selected=\"true\"" : $selected = "";
			$html    .= "<option $selected value=\"\">".$valorDefault."</option>";
		}
		$i = 0;
		foreach ($array as $info) {
			
			$comboValue	= $accessor->getValue($info, $codigo);
			
			if ( (is_array($codigoSel)) && (in_array($comboValue, $codigoSel))) {
				$selected = "selected=\"selected\"";
			}elseif ($codigoSel !== null) {
				($codigoSel == $accessor->getValue($info, $codigo)) ? $selected = "selected=\"true\"" : $selected = "";
			}else{
				if (($i == 0) && ($valorDefault === null) && !(is_array($codigoSel))) {
					$selected = "selected=\"true\"";
				}else{
					$selected = "";
				}
			}
			$html .= "<option value=\"".$comboValue."\" $selected>".$accessor->getValue($info, $valor).'</option>';
			$i++;
		}
		return ($html);
	}
            
    /**
     * Gerar o html da Combo do tipo Sim ou Não
     *
     * @return string
     */
    public function geraHtmlComboSN($codigoSel = null,$valorDefault = null) {
    	global $system,$tr;
    	
    	$array = array("0" => $tr->trans("Não"), "1" => $tr->trans("Sim"));
    
    	$html   = '';
    	if ($valorDefault !== null) {
    		($codigoSel == null) ? $selected = "selected=\"true\"" : $selected = "";
    		$html    .= "<option $selected value=\"\">".$valorDefault."</option>";
    	}
    	$i = 0;
    	foreach ($array as $val => $info) {
    		if ($codigoSel !== null) {
    			($codigoSel == $val) ? $selected = "selected=\"true\"" : $selected = "";
    		}else{
    			if (($i == 0) && ($valorDefault === null)) {
    				$selected = "selected=\"true\"";
    			}else{
    				$selected = "";
    			}
    		}
    		$html .= "<option value=\"".$val."\" $selected>".$info.'</option>';
    		$i++;
    	}
    	return ($html);
    }
    
    /**
     * Resgatar o html padrão
     * @return string
     */
    public function loadHtml() {
    	return $this->getDynHtmlLoad();
    }
    
    
    /**
     * Selecionar um módulo
     * @param number $codModulo
     */
    public function selecionaModulo($codModulo) {
    	global $log,$em,$_user,$_mod;
    	
    	if (self::temPermissaoNoModulo($codModulo) == false) return false;
    	
    	$_mod		= $em->getRepository('Entidades\ZgappModulo')->findOneBy(array ('codigo' => $codModulo));
    	
    	if ($_mod) {
    		//$log->debug("Usuário do módulog: ".$_user->getUsuario());
    		$_user->setUltModuloAcesso($_mod);
    		$em->persist($_user);
    		$em->flush();
    		$em->detach($_user);
    	}
    }
    
    /**
     * Verifica se o usuário tem permissão no menu
     */
    public function checaPermissao($codMenu) {
    	if ($this->temPermissaoNoMenu($codMenu) == false) \AppClass\App\Erro::halt('Sem Permissão no Menu !!!');
    }
    
    /**
     * Verifica se o usuário tem permissão no menu
     */
    public function temPermissaoNoMenu($codMenu) {
    	global $db,$system,$log;
    	
		$info	 = $db->extraiPrimeiro('SELECT * FROM `SLAPP_MENU` WHERE CODIGO = :codigo', array(':codigo' => $codMenu));
    	    	
    	if (!$info) return false;
    	
    	//if ($info->IND_FIXO == '1') return true;
    	
    	$query = $db->extraiTodos('SELECT M.* FROM `SLAPP_MENU` AS M
				LEFT JOIN `SLAPP_MENU_PERFIL` AS MP ON (M.CODIGO = MP.COD_MENU)
				LEFT JOIN `SLADM_PERFIL_ACESSO` AS PA ON (PA.CODIGO = MP.COD_TIPO_PERFIL)
				LEFT JOIN `SLSEG_USUARIO` AS U ON (U.COD_PERFIL = PA.CODIGO)
					WHERE M.CODIGO = :codMenu AND U.CODIGO = :codUsuario', 
    				array(':codUsuario' => $system->getCodUsuario(), 
    					':codMenu' => $codMenu,
    				));  	
    	
    	if (sizeof($query) > 0) return true;
    	
    	return false;
    }
    
    /**
     * @return the $codEmpresa
     */
    public function getCodEmpresa() {
    	return $this->codEmpresa;
    }
    
    /**
     * @param int $codEmpresa
     */
    public function setCodEmpresa($codEmpresa) {
    	$this->codEmpresa = $codEmpresa;
    }
    
    /**
     * @return the $codMatriz
     */
    public function getCodMatriz() {
    	return $this->codMatriz;
    }
    
    /**
     * @param int $codMatriz
     */
    public function setCodMatriz($codMatriz) {
    	$this->codMatriz = $codMatriz;
    }
    
    /**
     * @return the $codLang
     */
    public function getCodLang() {
    	return $this->codLang;
    }
    
    /**
     * @param int $codLang
     */
    public function setCodLang($codLang) {
    	$this->codLang = $codLang;
    }
    
    /**
     * @return the $codOrganizacao
     */
    public function getCodOrganizacao() {
    	return $this->codOrganizacao;
    }
    
    /**
     * @param int $codOrganizacao
     */
    public function setCodOrganizacao($codOrganizacao) {
    	$this->codOrganizacao = $codOrganizacao;
    }
    
    /**
     * @return the $codGrupoAcesso
     */
    public function getCodGrupoAcesso() {
    	return $this->codGrupoAcesso;
    }
    
    /**
     * @param int $codGrupoAcesso
     */
    public function setCodGrupoAcesso($codGrupoAcesso) {
    	$this->codGrupoAcesso = $codGrupoAcesso;
    }
    
}