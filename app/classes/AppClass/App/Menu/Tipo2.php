<?php

namespace AppClass\App\Menu;

/**
 * Gerenciar os menus do tipo 2 (menu Topo)
 *
 * @package \AppClass\App\Menu\Tipo2
 * @author Jalon Vitor Cerqueira Silva
 * @version 1.0.1
 */ 
class Tipo2 extends \AppClass\App\Menu\Tipo {
	
	/**
	 * Construtor
	 * 
	 * @param string $tipo        	
	 * @return void
	 */
	public function __construct($tipo) {
		parent::__construct($tipo);
	}
	
	
	/**
	 * Inicializa o código html, de acordo com o tipo do menu
	 */
	protected function iniciaHtml() {
		global $system,$_user,$_emp,$tr,$_org;
		
		/** Carrega as informações do usuário **/
		$avatar		= /*($_user->AVATAR) 	? $_user->AVATAR :*/ IMG_URL."/users/user-35.jpg";
		$nomeUsu	= /*($_user->APELIDO)	? $_user->APELIDO :*/ \AppClass\App\Util::getNomeCompleto($_user->NOME);
		
		$this->html .= '<main class="page-content content-wrap">
        <div class="navbar">
            <div class="navbar-inner">
                <div class="sidebar-pusher">
                    <a href="javascript:void(0);" class="waves-effect waves-button waves-classic push-sidebar">
                        <i class="fa fa-bars"></i>
                    </a>
                </div>
                <div class="logo-box">
                    <a href="/" class="logo-text" style="font-size: 15px;"><span>CT - Jalon Cabral</span></a>
                </div><!-- Logo Box -->
                <div class="search-button">
                    <a href="javascript:void(0);" class="waves-effect waves-button waves-classic show-search"><i class="fa fa-search"></i></a>
                </div>
                <div class="topmenu-outer">
                    <div class="top-menu">
                        <ul class="nav navbar-nav navbar-right">
                            <li>		
                                <a href="javascript:void(0);" class="waves-effect waves-button waves-classic toggle-fullscreen"><i class="fa fa-expand"></i></a>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li>	
                                <a href="javascript:void(0);" class="waves-effect waves-button waves-classic show-search"><i class="fa fa-search"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle waves-effect waves-button waves-classic" data-toggle="dropdown"><i class="fa fa-bell"></i><span class="badge badge-success pull-right">3</span></a>
                                <ul class="dropdown-menu title-caret dropdown-lg" role="menu">
                                    <li><p class="drop-title">You have 3 pending tasks !</p></li>
                                    <li class="dropdown-menu-list slimscroll tasks">
                                        <ul class="list-unstyled">
                                            <li>
                                                <a href="#">
                                                    <div class="task-icon badge badge-success"><i class="icon-user"></i></div>
                                                    <span class="badge badge-roundless badge-default pull-right">1min ago</span>
                                                    <p class="task-details">New user registered.</p>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="drop-all"><a href="#" class="text-center">All Tasks</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle waves-effect waves-button waves-classic" data-toggle="dropdown">
                                    <span class="user-name">'.$nomeUsu["primeiro_nome"].'<i class="fa fa-angle-down"></i></span>
                                    <img class="img-circle avatar" src="%PKG_URL%/assets/images/avatar1.png" width="40" height="40" alt="">
                                </a>
                                <ul class="dropdown-menu dropdown-list" role="menu">
                                    <li role="presentation"><a href="profile.html"><i class="fa fa-user"></i>Profile</a></li>
                                    <li role="presentation"><a href="calendar.html"><i class="fa fa-calendar"></i>Calendar</a></li>
                                    <li role="presentation"><a href="inbox.html"><i class="fa fa-envelope"></i>Inbox<span class="badge badge-success pull-right">4</span></a></li>
                                    <li role="presentation" class="divider"></li>
                                    <li role="presentation"><a href="lock-screen.html"><i class="fa fa-lock"></i>Lock screen</a></li>
                                    <li role="presentation"><a href="login.html"><i class="fa fa-sign-out m-r-xs"></i>Log out</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="%ROOT_URL%/Seg/logoff.php" class="log-out waves-effect waves-button waves-classic">
                                    <span><i class="fa fa-sign-out m-r-xs"></i>Log out</span>
                                </a>
                            </li>
                        </ul><!-- Nav -->
                    </div><!-- Top Menu -->
                </div>
            </div>
        </div><!-- Navbar -->';
	}
	
	/**
	 * Inicializa o código html, de acordo com o tipo do menu
	 */
	protected function finalizaHtml() {
		$this->html .= '
            <div class="page-footer">
            	<p class="no-s">2017 © Jalon Cerqueira</p>
            </div>
         </div><!-- Page Inner -->
     </main>';
	}
	
	/**
	 * Inicia o código html do menu lateral
	 */
	private function iniciaMenuTopo() {
		$this->html .= '<div class="horizontal-bar sidebar">
							<div class="page-sidebar-inner slimscroll">
								<ul class="menu accordion-menu">';
	}
	
	/**
	 * Finaliza o código html do menu lateral
	 */
	private function finalizaMenuTopo() {
		$this->html .= '		</ul>
							</div><!-- Page Sidebar Inner -->
						</div><!-- Page Sidebar -->';
	}
	
	/**
	 * Gerar o código html do menu
	 * @return void
	 */
	protected function geraHtml() {

		/**
		 * Inicializa o código HTML
		 */
		$this->iniciaHtml();
		$this->iniciaMenuTopo();
		//$this->iniciaMenuPadrao();
		//$this->iniciaMenuLateral();
		
		if ($this->_array) {
			foreach ($this->_array as $codigo => $array) {
				$this->geraHtmlItem($codigo,$array);
			}
		}
		$this->finalizaMenuTopo();
		$this->criaDivCentral();
		//$this->finalizaMenuPadrao();
		//$this->finalizaMenuLateral();
		
		/**
		 * Finaliza o código HTML
		 */
		$this->finalizaHtml();
	}
	
}
