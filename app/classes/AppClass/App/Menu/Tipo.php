<?php

namespace AppClass\App\Menu;

/**
 * Gerenciar os menus
 *
 * @package \AppClass\App\Menu\Tipo
 * @author Jalon Vitor Cerqueira Silva
 * @version 1.0.1
 */ 
abstract class Tipo {
	
	/**
	 * Tipos de menu
	 */
	const TIPO1 = "Lateral";
	const TIPO2 = "Topo";
	const TIPO3 = "Tipo3";
	
	/**
	 * Tipo do Menu
	 * @var string
	 */
	protected $tipo;
	
	/**
	 * Array de itens do Menu
	 * @var array
	 */
	protected $itens;
	
	/**
	 * Array dos itens fixos
	 * @var array
	 */
	protected $fixedItens;
	
	/**
	 * Código html do menu
	 * @var string
	 */
	protected $html;
	
	/**
	 * Href Padrão onde os menus serão abertos
	 * @var string
	 */
	protected $target;
	
	/**
	 * Url de gerencia de menu tipo2
	 * @var string
	 */
	protected $url;
	
	/**
	 * Array com a ordem correta dos itens do menu
	 * @var array
	 */
	protected $_array;
	
	
	/**
	 * Construtor
	 * 
	 * @param string $tipo        	
	 * @return void
	 */
	public function __construct($tipo) {

		/**
		 * Define o tipo do Menu
		 */
		switch ($tipo) {
			case self::TIPO1:
			case self::TIPO2:
				$this->setTipo($tipo);
				break;
			default:
				\AppClass\App\Erro::halt('Tipo de Menu desconhecido !!!');
		}
		
		/**
		 * Inicializa o array de itens
		 */
		$this->itens	= array();
		
	}
	
	/**
	 * Inicializa o código html, de acordo com o tipo do menu
	 */
	protected function iniciaMenuPadrao() {
		global $system,$_user,$_emp,$tr,$_org;

	}
	
	/**
	 * Monta as notificações
	 */
	protected function finalizaMenuPadrao() {
		global $system,$_user,$_emp,$tr,$_org;
	
		$this->html	.= '
 			<!-- Footer Start -->
			<br><br>
            <footer>
               Doador Online &copy; 2016
            </footer>
            <!-- Footer End -->			
            </div>
			<!-- ============================================================== -->
			<!-- End content here -->
			<!-- ============================================================== -->
        </div>
		<!-- End right content -->';
		
		/** Resgata o parâmetro de tempo de Atualização da notificação **/
		$timeout	= $system->config["atualizacao"]["notificacao"];
		if (!$timeout)	$timeout	= 5000;
		
		/** NOTIFICACAO **/
		$this->html	.= \AppClass\App\ZWS::NL.'<script type="text/javascript">'.\AppClass\App\ZWS::NL;
		$this->html	.= str_repeat(\AppClass\App\ZWS::TAB,1).'atualizaNotificacoes();'.\AppClass\App\ZWS::NL;
		//$this->html	.= str_repeat(\AppClass\App\ZWS::TAB,1).'atualizaMensagens();'.\AppClass\App\ZWS::NL;
		$this->html	.= str_repeat(\AppClass\App\ZWS::TAB,1).'var auto_refresh = setInterval('.\AppClass\App\ZWS::NL;
		$this->html	.= str_repeat(\AppClass\App\ZWS::TAB,2).'function () {'.\AppClass\App\ZWS::NL;
		$this->html	.= str_repeat(\AppClass\App\ZWS::TAB,3).'atualizaNotificacoes();'.\AppClass\App\ZWS::NL;
		//$this->html	.= str_repeat(\AppClass\App\ZWS::TAB,3).'atualizaMensagens();'.\AppClass\App\ZWS::NL;
		$this->html	.= str_repeat(\AppClass\App\ZWS::TAB,2).'}, '.$timeout.\AppClass\App\ZWS::NL;
		$this->html	.= str_repeat(\AppClass\App\ZWS::TAB,1).');'.\AppClass\App\ZWS::NL;
		$this->html	.= '</script>'.\AppClass\App\ZWS::NL;
		
		$this->html	.= '</div><!-- /.navbar-container -->'.\AppClass\App\ZWS::NL;
		
	}
	
	/**
	 * Cria o Iframe Central
	 */
	protected function criaDivCentral() {
		global $system;

		$this->html	.= str_repeat(\AppClass\App\ZWS::TAB,2).'<div class="page-inner">'.\AppClass\App\ZWS::NL;
		$this->html	.= str_repeat(\AppClass\App\ZWS::TAB,3).'<div class="content">'.\AppClass\App\ZWS::NL;
		$this->html	.= str_repeat(\AppClass\App\ZWS::TAB,4).'<div id="SpinnerID" class="hidden" style="width: 100%; height: 100%; position: fixed; display: block; opacity: 0.7; background-color: #fff; z-index: 80; text-align: left;">'.\AppClass\App\ZWS::NL;
		$this->html	.= str_repeat(\AppClass\App\ZWS::TAB,5).'<i class="fa fa-spinner fa-spin fa-3x"></i>'.\AppClass\App\ZWS::NL;
		$this->html	.= str_repeat(\AppClass\App\ZWS::TAB,4).'</div>'.\AppClass\App\ZWS::NL;
		$this->html	.= str_repeat(\AppClass\App\ZWS::TAB,4).'<div class="content" name="'.$system->getDivCentral().'" id="'.$system->getDivCentral().'ID"></div>'.\AppClass\App\ZWS::NL;
	}
	
	/**
	 * Adiciona uma pasta ao menu
	 * @param integer $codigo
	 * @param string $nome
	 * @param string $icone
	 * @param integer $itemPai
	 */
	public function adicionaPasta($codigo,$nome,$icone,$itemPai = null) {
		/**
		 * Verifica se o código já foi utilizado
		 */
		if ($this->existeItem($codigo) == true) {
			die('Código já existente ('.$codigo.')');
		}
		
		/**
		 * Verifica se o item Pai existe
		 */
		if ($this->getTipo() !== self::TIPO3) {
			if (($itemPai !== null) && ($this->existeItem($itemPai) == false) ) {
				die('Item Pai inexistente ('.$itemPai.')');
			}
		}
		
		/**
		 * Cria a pasta
		 */
		$classe					= "\\AppClass\\App\\Menu\\".$this->getTipo()."\\Pasta";
		$this->itens[$codigo]	= new $classe;
		$this->itens[$codigo]->setCodigo($codigo);
		$this->itens[$codigo]->setNome($nome);
		$this->itens[$codigo]->setIcone($icone);
		$this->itens[$codigo]->setitemPai($itemPai);
		
		if ($this->getTipo() == self::TIPO3) {
			$this->itens[$codigo]->setUrl($this->getUrl());
		}
	}
	
	/**
	 * Adiciona um Link ao menu
	 * @param integer $codigo
	 * @param string $nome
	 * @param string $icone
	 * @param string $url
	 * @param string $descricao
	 * @param integer $itemPai
	 */
	public function adicionaLink($codigo,$nome,$icone,$url,$descricao,$itemPai = null) {
		/**
		 * Verifica se o código já foi utilizado
		 */
		if ($this->existeItem($codigo) == true) {
			die('Código já existente ('.$codigo.')');
		}
	
		/**
		 * Verifica se o item Pai existe
		 */
		if ($this->getTipo() !== self::TIPO3) {
			if (($itemPai !== null) && ($this->existeItem($itemPai) == false) ) {
				die('Item Pai inexistente ('.$itemPai.')');
			}
		}
	
		/**
		 * Cria o link
		 */
		$classe					= "\\AppClass\\App\\Menu\\".$this->getTipo()."\\Link";
		$this->itens[$codigo]	= new $classe;
		$this->itens[$codigo]->setCodigo($codigo);
		$this->itens[$codigo]->setNome($nome);
		$this->itens[$codigo]->setIcone($icone);
		$this->itens[$codigo]->setUrl($url ? $url : $this->montaUrl($url, $codigo, $icone));
		$this->itens[$codigo]->setDescricao($descricao);
		$this->itens[$codigo]->setItemPai($itemPai);
		$this->itens[$codigo]->setTarget($this->getTarget());
		$this->itens[$codigo]->setId(self::geraId($url, $codigo,$icone));
	}
	
	
	/**
	 * Adiciona um Link ao menu
	 * @param integer $codigo
	 * @param integer $itemPai
	 */
	public function adicionaSeparador($codigo,$itemPai = null) {
		/**
		 * Verifica se o código já foi utilizado
		 */
		if ($this->existeItem($codigo) == true) {
			die('Código já existente ('.$codigo.')');
		}
	
		/**
		 * Verifica se o item Pai existe
		 */
		if (($itemPai !== null) && ($this->existeItem($itemPai) == false) ) {
			die('Item Pai inexistente ('.$itemPai.')');
		}
	
		/**
		 * Cria o Separador
		 */
		$classe					= "\\AppClass\\App\\Menu\\".$this->getTipo()."\\Separador";
		$this->itens[$codigo]	= new $classe;
		$this->itens[$codigo]->setCodigo($codigo);
		$this->itens[$codigo]->setItemPai($itemPai);
	
	}
	
	/**
	 * Adiciona um Link Fixo ao menu
	 * @param integer $codigo
	 * @param string $nome
	 * @param string $icone
	 * @param string $url
	 * @param string $descricao
	 */
	public function adicionaLinkFixo($codigo,$nome,$icone,$url,$descricao) {
		/**
		 * Verifica se o código já foi utilizado
		 */
		if ($this->existeItemFixo($codigo) == true) {
			die('Código Fixo já existente ('.$codigo.')');
		}
	
		/**
		 * Cria o link
		 */
		$classe					= "\\AppClass\\App\\Menu\\".$this->getTipo()."\\Link";
		$this->fixedItens[$codigo]	= new $classe;
		$this->fixedItens[$codigo]->setCodigo($codigo);
		$this->fixedItens[$codigo]->setNome($nome);
		$this->fixedItens[$codigo]->setIcone($icone);
		$this->fixedItens[$codigo]->setUrl($this->montaUrl($url, $codigo, $icone));
		$this->fixedItens[$codigo]->setDescricao($descricao);
		$this->fixedItens[$codigo]->setTarget($this->getTarget());
		$this->fixedItens[$codigo]->setId(self::geraId($url, $codigo, $icone));
	}
	
	
	/**
	 * Verifica se existe o item informado
	 * @param integer $codigo
	 * @return boolean
	 */
	protected function existeItem($codigo) {
		if (!$this->itens) return false;
		if (array_key_exists($codigo, $this->itens)) {
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Verifica se existe o item informado
	 * @param integer $codigo
	 * @return boolean
	 */
	protected function existeItemFixo($codigo) {
		if (!$this->fixedItens) return false;
		if (array_key_exists($codigo, $this->fixedItens)) {
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Montar a URL de um Menu
	 *
	 * @param String $link
	 * @param String $codItem
	 */
	public static function montaUrl($link,$codItem,$icone = null) {
		$id		= self::geraId($link, $codItem, $icone);
		$url	= $link."?id=".$id;
		return ($url);
	}
	
	/**
	 * Montar a URL Completa de um Menu
	 *
	 * @param String $link
	 * @param String $codItem
	 */
	public static function montaUrlCompleta($codMenu) {
	global $db,$system,$log;
	
    	try {
    		$info = $db->extraiPrimeiro('SELECT * FROM `SLAPP_MENU` WHERE CODIGO = :codMenu', 
    				array(':codMenu' => $codMenu));

    	}catch (\Exception $e) {
			\AppClass\App\Erro::halt($e->getMessage());
		}
		
		if (!$info) {
			return "#";
		}
		
		$modulo = (!$info->COD_MODULO) ? "Ext" : $info->COD_MODULO;
		
		if ($modulo == "Ext"){
			$url	= $info->LINK;
		}else{
			$url	= ROOT_URL . "/$modulo/" . self::montaUrl($info->LINK, $info->CODIGO, $info->ICONE);
		}
		
		return ($url);
	}
	
	
	/**
	 * Montar a URL de um Menu
	 *
	 * @param String $link
	 * @param String $codItem
	 */
	public static function geraId($link,$codItem,$icone = null) {
		/**
		 * verifica se a url já tem alguma variável
		 **/
		if (strpos($link,'?') !== false) {
			$vars	= '&'.substr(strstr($link, '?'),1);
			$link	= substr($link,0,strpos($link, '?'));
		}else{
			$vars	= '';
		}
			
		$id		= \AppClass\App\Util::encodeUrl("_codMenu_=".$codItem."&_icone_=".$icone.$vars);
		return ($id);
	}
	
	/** 
	 * Gera o array na ordem correta de nível e ordem
	 */
	protected function geraArray() {
		global $nivel,$nivelMax;

		/**
		 * Define os contadores para não deixar acontecer uma recursividade
		 */
		$nivel			= 0;
		$nivelMax		= 500;
		
		/** 
		 * Primeiro percorre o nível 0, os itens que não tem pai 
		 **/
		foreach ($this->itens as $codigo => $obj) {
			if ($this->getTipo() == self::TIPO3) {
				$this->_array[$codigo]	= array();
			}else{
				if ($obj->getItemPai() == null) {
					$this->_array[$codigo]	= array();
				}
			}
		}
		
		if ($this->_array) {
			/** 
			 * Encontrar os filhos 
			 **/
			foreach ($this->_array as $codigo => $array) {
				$this->descobreMenuFilhos($this->_array[$codigo], $codigo);			
			}
		
			/** 
			 * Definir os níveis dos menus 
			 **/
			foreach ($this->_array as $codigo => $array) {
				$this->itens[$codigo]->setNivel(0);
				$this->defineNivel($this->_array[$codigo],1);
			}
		
		}
		
		//print_r($this->_array);
	} 
	
	/**
	 * Descobre os filhos do $item no $this->_array e coloca em $array
	 * @param array $array
	 * @param string $item
	 */
	protected function descobreMenuFilhos(&$array,$item) {
		global $nivel,$nivelMax;
		$nivel++;
		foreach ($this->itens as $codigo => $obj) {
			if ($obj->getItemPai() == $item) {
				$array[$codigo] = array();
				$this->descobreMenuFilhos($array[$codigo], $codigo);
			}
			if ($nivel > $nivelMax) die('Recursividade encontrada em :'.__FUNCTION__);
		}
		
	}
	
	/**
	 * Definir o nível dos menus
	 * @param array $array
	 * @param Integer $nivel
	 */
	protected function defineNivel(&$array,$nivel) {
		foreach ($array as $cod => $arr) {
			$this->itens[$cod]->setNivel($nivel);
			if (!empty($arr)) {
				$this->defineNivel($array[$cod], $nivel+1);
			}
		}
	}
	
	/**
	 * Gerar o código html do menu
	 * @return void
	 */
	protected function geraHtmlItem($codigo,$array) {
		global $log;
		$clPasta	= "\\AppClass\\App\\Menu\\".$this->getTipo()."\\Pasta";
		$clLink		= "\\AppClass\\App\\Menu\\".$this->getTipo()."\\Link";
		$clSep		= "\\AppClass\\App\\Menu\\".$this->getTipo()."\\Separador";
		
		if ($this->itens[$codigo] instanceof $clPasta) {
			$this->html .= $this->itens[$codigo]->abrirTag();
			$this->html .= $this->itens[$codigo]->geraHtml();
			if (!empty($array)) {
				foreach ($array as $cod => $arr) {
					$this->geraHtmlItem($cod, $arr);
				}
			}
			$this->html .= $this->itens[$codigo]->fecharTag();
		}elseif ($this->itens[$codigo] instanceof $clLink) {
			$this->html .= $this->itens[$codigo]->geraHtml();
		}elseif ($this->itens[$codigo] instanceof $clSep) {
			$this->html .= $this->itens[$codigo]->geraHtml();
		}
		
	}	
	
	/**
	 * @return the $tipo
	 */
	protected function getTipo() {
		return $this->tipo;
	}

	/**
	 * @param string $tipo
	 */
	protected function setTipo($tipo) {
		$this->tipo = $tipo;
	}

	/**
	 * @return the $html
	 */
	public function getHtml() {
		$this->geraArray();
		$this->geraHtml();
		return $this->html;
	}
	
	/**
	 * @return the $target
	 */
	public function getTarget() {
		return $this->target;
	}

	/**
	 * @param string $target
	 */
	public function setTarget($target) {
		$this->target = $target;
	}
	
	/**
	 * @return the $url
	 */
	public function getUrl() {
		return $this->url;
	}

	/**
	 * @param string $url
	 */
	public function setUrl($url) {
		$this->url = $url;
	}

}
