<?php

namespace AppClass\App\Validador;

/**
 * Validador de E-Mail
 *
 * @package \AppClass\App\Validador\Email
 * @author Jalon Vitor Cerqueira Silva
 * @version 1.0.1
 */
class Email extends \Zend\Validator\EmailAddress {
	
	/**
	 * 
	 */
	public function __construct() {
	
	}

}