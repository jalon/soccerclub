<?php

namespace AppClass\App;

/**
 * Gerenciar os menus
 *
 * @package \AppClass\App\Menu
 * @author Jalon Vítor Cequeira Silva
 * @version 1.0.1
 */ 
class Menu {
	
	/**
	 * Objeto que irá guardar a instância
	 */
	private static $instance;
	
	/**
	 * Construtor
	 *
	 * @return object
	 */
	public static function criar($tipo) {
		/**
		 * Define o tipo do Menu
		 */
		switch ($tipo) {
			case \AppClass\App\Menu\Tipo::TIPO1:
				self::$instance	= new \AppClass\App\Menu\Tipo1($tipo);
				break;
			case \AppClass\App\Menu\Tipo::TIPO2:
				self::$instance	= new \AppClass\App\Menu\Tipo2($tipo);
				break;
			default:
				\AppClass\App\Erro::halt('Tipo de Menu desconhecido !!!');
		}
		return self::$instance;
	}
	
	/**
	 * Construtor privado, usar \AppClass\App\Menu::getInstance();
	 */
	private function __construct($tipo,$nome) {
	
	}

}
