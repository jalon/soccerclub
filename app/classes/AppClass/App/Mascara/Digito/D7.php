<?php

namespace AppClass\App\Mascara\Digito;

/**
 * Gerenciar os Digito de uma Máscara
 *
 * @package \AppClass\App\Mascara\Digito\D7
 * @created 31/08/2014
 * @author Jalon Vitor Cerqueira Silva
 * @version 1.0.1
 *         
 */
class D7 extends \AppClass\App\Mascara\Digito {
	
	/**
	 * Construtor
	 */
	public function __construct() {
		
		parent::__construct();

		/**
		 * Define as configurações do dígito
		 */
		$this->setDigito("7");
		$this->setPattern("[0-7]");
		$this->setOpcional(false);
		$this->recursivo(false);
		
	}

}
