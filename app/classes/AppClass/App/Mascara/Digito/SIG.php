<?php

namespace AppClass\App\Mascara\Digito;

/**
 * Gerenciar os Digito de uma Máscara
 *
 * @package \AppClass\App\Mascara\Digito\SIG
 * @author Jalon Vitor Cerqueira Silva
 * @version 1.0.1
 *         
 */
class SIG extends \AppClass\App\Mascara\Digito {
	
	/**
	 * Construtor
	 */
	public function __construct() {
		
		parent::__construct();

		/**
		 * Define as configurações do dígito
		 */
		$this->setDigito("~");
		$this->setPattern("[+-]");
		$this->setOpcional(true);
		$this->recursivo(false);
	}

}
