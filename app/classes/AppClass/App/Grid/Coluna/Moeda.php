<?php

namespace AppClass\App\Grid\Coluna;

/**
 * Gerenciar as colunas to tipo Moeda
 *
 * @package Moeda
 *          @created 23/11/2013
 * @author Jalon Vitor Cerqueira Silva
 * @version 1.0
 *         
 */
class Moeda extends \AppClass\App\Grid\Coluna {
	
	/**
	 * Construtor
	 */
	public function __construct() {
		parent::__construct ();
		
		$this->setTipo ( \AppClass\App\Grid\Tipo::TP_MOEDA );
	}
	
	/**
	 * Gerar o código Html da célula
	 */
	public function geraHtmlValor($valor) {
		$html = $valor;
		return ($html);
	}
}
