<?php

namespace AppClass\App\Grid\Coluna;

/**
 * Gerenciar as colunas to tipo Texto
 *
 * @package Texto
 * @created 20/06/2013
 * @author Jalon Vitor Cerqueira Silva
 * @version 1.0
 *         
 */
class Texto extends \AppClass\App\Grid\Coluna {
	
	/**
	 * Construtor
	 */
	public function __construct() {
		parent::__construct ();
		
		$this->setTipo ( \AppClass\App\Grid\Tipo::TP_TEXTO );
	}
	
	/**
	 * Gerar o código Html da célula
	 */
	public function geraHtmlValor($valor) {
		if ($this->getMascara()) {
			$html = \AppClass\App\Mascara::tipo($this->getMascara())->aplicaMascara($valor);
		}else{
			$html = $valor;
		}
		
		return ($html);
	}
}
