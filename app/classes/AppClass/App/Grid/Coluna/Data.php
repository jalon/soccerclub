<?php

namespace AppClass\App\Grid\Coluna;

/**
 * Gerenciar as colunas to tipo Data
 *
 * @package Data
 * @author Jalon Vitor Ceruqueira Silva
 * @version 1.0
 *         
 */
class Data extends \AppClass\App\Grid\Coluna {
	
	/**
	 * Construtor
	 */
	public function __construct() {
		parent::__construct ();
		
		$this->setTipo ( \AppClass\App\Grid\Tipo::TP_DATA );
	}
	
	/**
	 * Gerar o código Html da célula
	 */
	public function geraHtmlValor($valor) {
		$html = $valor;
		return ($html);
	}
}
