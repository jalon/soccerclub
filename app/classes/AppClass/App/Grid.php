<?php

namespace AppClass\App;

/**
 * Gerenciar os grids em bootstrap
 *
 * @package \AppClass\App\Grid
 * @created 20/03/2013
 * @author Jalon Vitor Cerqueira
 * @version 1.0.2
 *         
 */
class Grid  {
	
	/**
	 * Objeto que irá guardar a instância
	 */
	private static $instance;
	
	/**
	 * Construtor 
	 *
	 * @return object
	 */
	public static function criar($tipo,$nome) {
		
		/**
		 * Verifica se o tipo do Grid é válido
		 */
		switch ($tipo) {
			case \AppClass\App\Grid\Tipo::TP_BOOTSTRAP:
				self::$instance	= new \AppClass\App\Grid\Tipo\Bootstrap($nome);
				break;
			case \AppClass\App\Grid\Tipo::TP_DHTMLX:
				self::$instance	= new \AppClass\App\Grid\Tipo\DHTMLX($nome);
				break;
			default:
				die ('Tipo de grid não implementado !!!');
				break;
		}
		
		return self::$instance;
	}
	
	/**
	 * Construtor privado, usar \AppClass\App\Grid::getInstance();
	 */
	private function __construct($tipo,$nome) {
		
	}
	

}
