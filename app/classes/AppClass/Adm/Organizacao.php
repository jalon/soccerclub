<?php

namespace AppClass\Adm;

/**
 * Organização
 * 
 * @package: Organizacao
 * @Author: Jalon V�tor Cerqueira Silva
 * @version: 1.0.1
 * 
 */

class Organizacao {

	/**
     * Construtor
     *
	 * @return void
	 */
	private function __construct() {
		global $log;
		$log->debug(__CLASS__.": nova Instância");
	}
	
    /**
     * Buscar uma organização através da identificação
     *
     * @param integer $ident
     * @return array
     */
	public static function buscaPorIdentificacao($ident) {
		global $db,$system,$log;
	
		try {
			$smt = $db->extraiPrimeiro('SELECT * FROM `SLADM_ORGANIZACAO` WHERE IDENTIFICACAO = :ident', array(':ident' => strtoupper($ident) ));
			
			return($smt);
		} catch (\Exception $e) {
			\AppClass\App\Erro::halt($e->getMessage());
		}
			
	}
	
	/**
	 * Buscar uma organização através do codigo
	 *
	 * @param integer $ident
	 * @return array
	 */
	public static function buscaPorCodigo($codigo) {
		global $db,$system,$log;
	
		try {
			$smt = $db->extraiPrimeiro('SELECT * FROM `SLADM_ORGANIZACAO` WHERE CODIGO = :codigo', 
						array(':codigo' => $codigo) );
				
			return($smt);
		} catch (\Exception $e) {
			\AppClass\App\Erro::halt($e->getMessage());
		}
			
	}
	
	/**
	 * Listar todas as organizacoes
	 *
	 * @param integer $ident
	 * @return array
	 */
	public static function listarTodas() {
		global $db,$system,$log;
	
		try {
			$smt = $db->extraiTodos('SELECT O.*, E.NOME AS ESTADO, C.NOME AS CIDADE, OC.NUMERO FROM `SLADM_ORGANIZACAO` AS O
					LEFT OUTER JOIN `SLADM_ESTADO` E ON (O.COD_ESTADO = E.COD_UF)
					LEFT OUTER JOIN `SLADM_CIDADE` C ON (O.COD_CIDADE = C.CODIGO)
					LEFT OUTER JOIN `SLADM_ORGANIZACAO_CONTATO` OC ON (O.CODIGO = OC.COD_ORGANIZACAO)
						ORDER BY IDENTIFICACAO ASC', array());
				
			return($smt);
		} catch (\Exception $e) {
			\AppClass\App\Erro::halt($e->getMessage());
		}
			
	}
	
}
