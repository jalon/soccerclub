<?php
#################################################################################
## Includes
#################################################################################
if (defined('DOC_ROOT')) {
	include_once(DOC_ROOT . 'includeNoAuth.php');
}else{
	include_once('../includeNoAuth.php');
}

$system->setCodLang(1);
	
include_once(BIN_PATH . 'auth.php');

#################################################################################
## Resgata a vari�vel ID que est� criptografada
#################################################################################
if (isset($_GET['id'])) {
	$id = \AppClass\App\Util::antiInjection($_GET["id"]);
}elseif (isset($_POST['id'])) {
	$id = \AppClass\App\Util::antiInjection($_POST["id"]);
}else{
	$id	= null;
}

#################################################################################
## Descompacta o ID
#################################################################################
\AppClass\App\Util::descompactaId($id);

#################################################################################
## Descobre o m�dulo que ser� iniciado
#################################################################################
if (isset($_codModulo_)) {
	$system->selecionaModulo($_codModulo_);
	$codModulo	= $_codModulo_;
}else{
	$codModulo	= null;
	$urlInicial	= ROOT_URL . "/App/modulo.php?id=";
}

#################################################################################
## Abrir o index
#################################################################################
$urlInicial 	= \AppClass\App\Menu\Tipo::montaUrlCompleta(1); // Codigo Dashboard

#################################################################################
## Define a constante do site inicial
#################################################################################
$system->setHomeUrl($_SERVER['REQUEST_URI']);

#################################################################################
## Define o nome do iframe Central
#################################################################################
$system->setDivCentral("divContent");

#################################################################################
## Cria o objeto do Menu
#################################################################################
$tipoMenu	= \AppClass\App\Menu\Tipo::TIPO2;

$menu	= \AppClass\App\Menu::criar($tipoMenu);
$menu->setTarget($system->getDivCentral());

#################################################################################
## Carrega os menus do m�dulo
#################################################################################
$menus 	= \AppClass\Seg\Usuario::listaMenusAcesso($_user->CODIGO);

#################################################################################
## Adiciona os menus no objeto
#################################################################################
if ($menus) {
	foreach ($menus as $dados) {
		if ($dados->COD_TIPO == "M") {
			$codMenuPai	= $dados->COD_PAI ? $dados->COD_PAI : null;  
			$menu->adicionaPasta($dados->CODIGO, $dados->NOME, $dados->ICONE, $codMenuPai);
		}elseif ($dados->COD_TIPO == "L") {
			$url 	= \AppClass\App\Menu\Tipo::montaUrlCompleta($dados->CODIGO);
			$codMenuPai	= $dados->COD_PAI ? $dados->COD_PAI : null;
			$menu->adicionaLink($dados->CODIGO, $dados->NOME, $dados->ICONE, $url, $dados->DESCRICAO, $codMenuPai);
		}else{
			die('Tipo de Menu desconhecido');
		}
	}
}

#################################################################################
## Carregando o template html
#################################################################################
$tpl	= new \AppClass\App\Template();
$tpl->load(HTML_PATH . "/index.html");

#################################################################################
## Define os valores das vari�veis
#################################################################################
$tpl->set('CODE_HTML'			,$menu->getHtml());
$tpl->set('DIVCENTRAL'			,$system->getDivCentral());
$tpl->set('URLINICIAL'			,$urlInicial);

#################################################################################
## Por fim exibir a p�gina HTML
#################################################################################
$tpl->show();

?>