<?php
global $log,$system,$db,$em,$_user,$_mod;

#################################################################################
## Includes
#################################################################################
if (defined('DOC_ROOT')) {
	include_once(DOC_ROOT . 'includeNoAuth.php');
}else{
	include_once('../includeNoAuth.php');
}

#################################################################################
## Verificar se o usuário e senha foram passados através do form
#################################################################################
if ((isset($_POST['usuario'])) && (isset($_POST['senha']))) {
	$_usuario		= \AppClass\App\Util::antiInjection($_POST['usuario']);
	$_senha			= \AppClass\App\Util::antiInjection($_POST['senha']);
	
	$_senhaCrip		= \AppClass\App\Crypt::crypt($_usuario, $_senha);
}else{
	$_usuario		= '';
	$_senha			= '';
}

#$system->desautentica();

#################################################################################
## Limpando a variável da mensagem
#################################################################################
$mensagem		= '';

#################################################################################
## Verifica se o usuário já está conectado
#################################################################################
if (!$system->estaAutenticado()) {

	if (($_usuario) && ($_senha)) {

		$valUsuario	= new \AppClass\Seg\Auth\validaUsuario();
		$valSenha	= new \AppClass\Seg\Auth\validaSenha();
		
		if (!$valUsuario->isValid($_usuario)) {
    		$r			= \Zend\Authentication\Result::FAILURE_CREDENTIAL_INVALID;
    		$result		= new \Zend\Authentication\Result($r, $_usuario, $valUsuario->getMessages());
		}elseif (!$valSenha->isValid($_senha)) {
    		$r			= \Zend\Authentication\Result::FAILURE_CREDENTIAL_INVALID;
    		$result		= new \Zend\Authentication\Result($r, $_usuario, $valSenha->getMessages());
		}else{
			$authAdap	= new \AppClass\Seg\Auth($_usuario, $_senhaCrip);
			$result		= $authAdap->authenticate();
		}
		
		if (!$result->isValid()) {

			$m			= $result->getMessages();
			if (isset($m[0])) {
				$mensagem	= $m[0];
			}else{
				$mensagem	= "Usuário / senha inconsistentes !!!";
			}
			
			$log->debug('Usuário/Senha incorretos !!! ');
			
			include_once(MOD_PATH . '/Seg/php/login.php');
			exit;
		} else {
			/** Autenticação OK **/
			$log->debug('Usuário autenticado com sucesso !!! ');
			
			try {
				
				/** Resgata os dados do usuário **/
				$_user	 = $db->extraiPrimeiro('SELECT U.* FROM `SLSEG_USUARIO` AS U 
							WHERE U.USUARIO = :usuario', array(':usuario' => $_usuario ));
				$system->setCodUsuario($_user->CODIGO);
				$system->setCodPessoa($_user->COD_PESSOA);
				$system->setAutenticado();
				/** Ultimo acesso falta implementar **/
				//$system->setDataUltAcesso($_user->getDataUltAcesso());
				
				/** Resgata novamente os dados do usuário atualizado **/
				$_user	 = $db->extraiPrimeiro('SELECT U.*, P.COD_SEXO, P.NOME FROM `SLSEG_USUARIO` AS U LEFT JOIN `SLSEG_PESSOA` P ON (U.COD_PESSOA = P.CODIGO) WHERE U.USUARIO = :usuario', array(':usuario' => $_usuario ));
			} catch (\Exception $e) {
				\AppClass\App\Erro::halt($e->getMessage(),__FILE__,__LINE__);
			}
			
			#echo "Login efetuado com sucesso!";
		}
	}else{
		#header('Location: /');
		include_once(MOD_PATH . '/Seg/php/login.php');
		exit;
	}
}else{
	/** Resgata os dados do usuário **/
	$_user	 = $db->extraiPrimeiro('SELECT U.*, P.COD_SEXO, P.NOME FROM `SLSEG_USUARIO` AS U LEFT JOIN `SLSEG_PESSOA` P ON (U.COD_PESSOA = P.CODIGO) WHERE U.CODIGO = :codUser', array(':codUser' => $system->getCodUsuario() ));
}

#################################################################################
## Define o usuário que está logado no banco
#################################################################################
$db->setLoggedUser($_user->CODIGO);

?>