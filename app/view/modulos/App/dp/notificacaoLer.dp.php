<?php
#################################################################################
## Includes
#################################################################################
if (defined('DOC_ROOT')) {
	include_once(DOC_ROOT . 'include.php');
}else{
 	include_once('../include.php');
}
 
#################################################################################
## Resgata os parâmetros passados pelo formulário
#################################################################################
if (isset($_POST['codSolicitacao']))	$codSolicitacao		= \AppClass\App\Util::antiInjection($_POST['codSolicitacao']);

#################################################################################
## Limpar a variável de erro
#################################################################################
$err		  = false;
$msgErr 	  = null;

#################################################################################
## Fazer validação dos campos
#################################################################################
/** COD_SOLICITACAO **/
if ((empty($codSolicitacao))) {
	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans("COD_SOLICITACAO é obrigatório"));
	$err	= 1;
}

$info	 = $db->extraiPrimeiro('SELECT S.* FROM `SLREC_SOLICITACAO` S
									WHERE S.CODIGO =:codSolicitacao', array(':codSolicitacao' => $codSolicitacao ));

if(isset($info->scalar) && $info->scalar == false) {
	$msgErr = "SOLICITACAO não existe!";
	$err	= 1;
}else if( $info->COD_STATUS == "C" ){
	$msgErr = "Essa SOLICITACAO foi cancelada pelo usuário!";
	$err	= 1;
}else if( $info->COD_STATUS == "A" || $info->COD_STATUS == "R" ){
	$msgErr = "Essa SOLICITACAO já foi aceita por outro usuário!";
	$err	= 1;
}

if ($err != null) {
	echo '1| '.$msgErr;
 	exit;
}
 
#################################################################################
## Salvar no banco
#################################################################################
try {
	
	$oAceito	= $db->Executa('INSERT INTO `SLDOA_SOLICITACAO_ACEITA`(`COD_USUARIO_ACEITO`, `COD_SOLICITACAO`, `DATA_ACEITO`)
				VALUES (:codUsuario,:codSolicitacao,:dataAceito)',
			array(':codUsuario' => $system->getCodUsuario(),
					':codSolicitacao' => $codSolicitacao,
					':dataAceito' => date('Y-m-d')
			)
	);
	
	$oSolicitacao	= $db->Executa('UPDATE `SLREC_SOLICITACAO` SET `COD_STATUS`=:codStatus WHERE CODIGO =:codigo',
			array(':codigo' => $codSolicitacao,
					':codStatus' => "A"
			)
	);
	
} catch (\Exception $e) {
 	echo '1| Ocorreu um erro ao aceitar o pedido, tente mais tarde ou entre em contato conosco.';
 	exit;
}
 
$system->criaAviso(\AppClass\App\Aviso\Tipo::INFO, "Pedido de doação aceito com sucesso. <br> Para saber mais sobre quem irá receber sua doação, acesse <u>Informações</u> em pedidos aceitos.");
echo '0'.\AppClass\App\Util::encodeUrl('|'.$codSolicitacao);