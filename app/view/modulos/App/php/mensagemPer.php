<?php
################################################################################
# Includes
################################################################################
if (defined ( 'DOC_ROOT' )) {
	include_once (DOC_ROOT . 'include.php');
} else {
	include_once ('../include.php');
}

################################################################################
# Resgata a variável ID que está criptografada
################################################################################
if (isset ( $_GET ['id'] )) {
	$id = \AppClass\App\Util::antiInjection ( $_GET ["id"] );
} elseif (isset ( $_POST ['id'] )) {
	$id = \AppClass\App\Util::antiInjection ( $_POST ["id"] );
} elseif (isset ( $id )) {
	$id = \AppClass\App\Util::antiInjection ( $id );
} else {
	\AppClass\App\Erro::halt ( 'Falta de Parâmetros' );
}

################################################################################
# Descompacta o ID
################################################################################
\AppClass\App\Util::descompactaId ( $id );

################################################################################
# Verifica se o usuário tem permissão no menu
################################################################################
$system->checaPermissao ( $_codMenu_ );

################################################################################
# Resgata as informações do banco
################################################################################
if ($codNotifUsuario) {
	try {
		$info	 = $db->extraiPrimeiro('SELECT N.MENSAGEM, NU.COD_USUARIO_ENVIOU, S.RELATO, NU.IND_LIDA FROM `SLNOT_NOTIFICACAO_USUARIO` AS NU
				LEFT OUTER JOIN `SLNOT_NOTIFICACAO` N ON (NU.COD_NOTIFICACAO = N.CODIGO)
				LEFT OUTER JOIN `SLREC_SOLICITACAO` S ON (N.COD_SOLICITACAO = S.CODIGO)
					WHERE NU.CODIGO = :codigo', 
				array(':codigo' => $codNotifUsuario));
	} catch ( \Exception $e ) {
		\AppClass\App\Erro::halt ( $e->getMessage () );
	}
	
	$codUsuario		 = ($info->COD_USUARIO_ENVIOU) ? $info->COD_USUARIO_ENVIOU : null;
	$relato			 = ($info->RELATO) ? $info->RELATO : null;
	$mensagem		 = ($info->MENSAGEM) ? $info->MENSAGEM : $relato;
	$indLida		 = ($info->IND_LIDA) ? $info->IND_LIDA : null;
	
	## Mudar status para lida
	if($indLida == 0)
		$oLer	= $db->Executa('UPDATE `SLNOT_NOTIFICACAO_USUARIO` SET `IND_LIDA`=:indLida,`DATA_LIDA`=:dataLida WHERE `CODIGO`=:codigo',
		array('codigo' => $codNotifUsuario, 'indLida' => 1, 'dataLida' => date('Y-m-d H:i:s') ));
} else {
	$codUsuario	 	 = null;
	$mensagem		 = null;
}

################################################################################
# Url Voltar
################################################################################
$urlVoltar = ROOT_URL . "/App/meusDadosLis.php?id=" . $id;

################################################################################
# Carregando o template html
################################################################################
$tpl = new \AppClass\App\Template ();
$tpl->load ( \AppClass\App\Util::getCaminhoCorrespondente ( __FILE__, \AppClass\App\ZWS::EXT_HTML ) );

################################################################################
# Define os valores das variáveis
################################################################################
$tpl->set ( 'URL_FORM'			   , $_SERVER ['SCRIPT_NAME'] );
$tpl->set ( 'URLVOLTAR'			   , $urlVoltar );
$tpl->set ( 'ID'				   , $id );
$tpl->set ( 'COD_USUARIO'		   , $codUsuario);
$tpl->set ( 'MENSAGEM'			   , $mensagem);

$tpl->set ( 'DP', \AppClass\App\Util::getCaminhoCorrespondente ( __FILE__, \AppClass\App\ZWS::EXT_DP, \AppClass\App\ZWS::CAMINHO_RELATIVO ) );
################################################################################
# Por fim exibir a página HTML
################################################################################
$tpl->show ();

