<?php
#################################################################################
## Includes
#################################################################################
if (defined('DOC_ROOT')) {
	include_once(DOC_ROOT . 'include.php');
}else{
	include_once('../include.php');
}

#################################################################################
## Resgata a variável ID que está criptografada
#################################################################################
if (isset($_GET['id'])) {
	$id = \AppClass\App\Util::antiInjection($_GET["id"]);
}elseif (isset($_POST['id'])) {
	$id = \AppClass\App\Util::antiInjection($_POST["id"]);
}elseif (isset($id)) 	{
	$id = \AppClass\App\Util::antiInjection($id);
}else{
	\AppClass\App\Erro::halt('Falta de Parâmetros');
}

#################################################################################
## Descompacta o ID
#################################################################################
\AppClass\App\Util::descompactaId($id);

#################################################################################
## Verifica se o usuário tem permissão no menu
#################################################################################
$system->checaPermissao($_codMenu_);

#################################################################################
## Resgata a url desse script
#################################################################################
$url		= ROOT_URL . '/App/'. basename(__FILE__);

#################################################################################
## Resgata os dados do grid
#################################################################################
try {
	$usuario	 = $db->extraiTodos('SELECT P.* FROM `SLSEG_PESSOA` AS P 
				LEFT OUTER JOIN `SLSEG_USUARIO` AS U ON (P.CODIGO = U.COD_PESSOA) ORDER BY P.NOME ASC', array());
} catch (\Exception $e) {
	\AppClass\App\Erro::halt($e->getMessage());
}
	
#################################################################################
## Cria o objeto do Grid (bootstrap)
#################################################################################
$grid			= \AppClass\App\Grid::criar(\AppClass\App\Grid\Tipo::TP_BOOTSTRAP,"GCargo");
$grid->adicionaTexto($tr->trans('NOME'),	 		15, $grid::CENTER	,'NOME');
$grid->adicionaBotao(\AppClass\App\Grid\Coluna\Botao::MOD_EDIT);
$grid->adicionaBotao(\AppClass\App\Grid\Coluna\Botao::MOD_REMOVE);
$grid->importaDadosDoctrine($usuario);

#################################################################################
## Popula os valores dos botões
#################################################################################
for ($i = 0; $i < sizeof($usuario); $i++) {
	$uid		= \AppClass\App\Util::encodeUrl('_codMenu_='.$_codMenu_.'&_icone_='.$_icone_.'&codUsuario='.$usuario[$i]->CODIGO.'&url='.$url);
	
	$grid->setUrlCelula($i,1,ROOT_URL.'/App/meusDadosCad.php?id='.$uid);
	$grid->setUrlCelula($i,2,'javascript:AbreModal(\'/App/meusDadosExc.php?id='.$uid.'\');"');
}

#################################################################################
## Gerar o código html do grid
#################################################################################
try {
	$htmlGrid	= $grid->getHtmlCode();
} catch (\Exception $e) {
	\AppClass\App\Erro::halt($e->getMessage());
}

#################################################################################
## Gerar a url de adicão
#################################################################################
$urlAdd			= ROOT_URL.'/App/meusDadosCad.php?id='.\AppClass\App\Util::encodeUrl('_codMenu_='.$_codMenu_.'&_icone_='.$_icone_.'&codUsuario=');

#################################################################################
## Carregando o template html
#################################################################################
$tpl	= new \AppClass\App\Template();
$tpl->load(HTML_PATH . 'templateLis.html');

#################################################################################
## Define os valores das variáveis
#################################################################################
$tpl->set('GRID'			,$htmlGrid);
$tpl->set('NOME'			,$tr->trans('Meus Dados'));
$tpl->set('URLADD'			,$urlAdd);
$tpl->set('IC'				,$_icone_);

#################################################################################
## Por fim exibir a página HTML
#################################################################################
$tpl->show();
