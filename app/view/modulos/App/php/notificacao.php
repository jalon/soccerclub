<?php
#################################################################################
## Includes
#################################################################################
if (defined('DOC_ROOT')) {
	include_once(DOC_ROOT . 'include.php');
}else{
	include_once('../include.php');
}

#################################################################################
## Variáveis globais
#################################################################################
global $system,$tr,$log,$db;

#################################################################################
## Resgata as notificações 
#################################################################################
$notificacoes 	= \AppClass\App\Notificacao::listarPendentes();

#################################################################################
## Gera o código html da notificação
#################################################################################
$numNot			= sizeof($notificacoes);
$html			= "";
$html .= str_repeat(\AppClass\App\ZWS::TAB,6).'<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i>'.\AppClass\App\ZWS::NL;

$html .= str_repeat(\AppClass\App\ZWS::TAB,6).'<span class="label label-danger absolute">'.$numNot.'</span>'.\AppClass\App\ZWS::NL;
$html .= str_repeat(\AppClass\App\ZWS::TAB,6).'<ul class="dropdown-menu dropdown-message">'.\AppClass\App\ZWS::NL;
$html .= str_repeat(\AppClass\App\ZWS::TAB,7).'<li class="dropdown-header notif-header"><i class="icon-bell-2"></i>Notificações<a class="pull-right" href="#"><i class="fa fa-cog"></i></a></li>'.\AppClass\App\ZWS::NL;

if ($numNot > 0) {
	foreach ($notificacoes as $not) {
		/*if ($not->getCodRemetente()) {
			$avatar	= ($not->getCodRemetente()->getAvatar()) ? $not->getCodRemetente()->getAvatar()->getLink() : IMG_URL."/avatars/usuarioGenerico.png";
			$nome	= $not->getCodRemetente()->getApelido();
		}else{
			$avatar	= IMG_URL."/avatars/usuarioGenerico.png";
			$nome	= "Anônimo";
		}
		$temAnexo		= \AppClass\App\Notificacao::temAnexo($not->getCodigo());
		
		if ($temAnexo)	{
			$data		= $not->getData()->format($system->config["data"]["datetimeFormat"]) . "&nbsp;<i class='fa fa-paperclip blue fa-lg'></i>";
		}else{
			$data		= $not->getData()->format($system->config["data"]["datetimeFormat"]);
		}*/
		
		$linkTodos	= ROOT_URL."/App/notificacaoLis.php";
		//$avatar		= str_replace("%IMG_URL%", IMG_URL, $avatar);
		//$avatar		= str_replace("%PKG_URL%", PKG_URL, $avatar);
		
		$lid	= \AppClass\App\Util::encodeUrl('_codMenu_=12&_icone_=fa fa-bell&codNotifUsuario='.$not->COD_NOTIF_USUARIO);
		
		if( $not->COD_TIPO == "M" ){
			$linkAbrir = 'javascript:AbreModal(\'/App/mensagemPer.php?id='.$lid.'\');';
		}else {
			$linkAbrir = 'javascript:AbreModal(\'/App/notificacaoLer.php?id='.$lid.'\');';
		}
		
		$html .= str_repeat(\AppClass\App\ZWS::TAB,7).
		'<li class="unread">
			<a href="'.$linkAbrir.'" class="clearfix">
         		<p><strong>Pessoa #'.$not->COD_USUARIO_ENVIOU.':</strong> '.$not->ASSUNTO.'
					<br />'.$not->MENSAGEM.'
					<br /><i>'.\AppClass\App\Util::formatData($system->config["data"]["datetimeSimplesFormat"], $not->DATA_CRIADA).'</i>
		        </p>	
			</a>
         </li>'.\AppClass\App\ZWS::NL;
	}
} else {
	$html .= str_repeat(\AppClass\App\ZWS::TAB,7).
	'<li class="unread">
		<a class="clearfix">
			<p>Que pena não temos nada de novo por aqui :(</p>
		</a>
    </li>'.\AppClass\App\ZWS::NL;
}

$nid	= \AppClass\App\Util::encodeUrl('_codMenu_=12&_icone_=fa fa-bell');

$html .= str_repeat(\AppClass\App\ZWS::TAB,7).
'<li class="dropdown-footer">
	<div class="btn-group btn-group-justified">
		<div class="btn-group">
			<button type="button" onclick="atualizaNotificacoes();" class="btn btn-sm btn-primary"><i class="icon-ccw-1"></i> Recarregar</button>
		</div>
		<!--<div class="btn-group">
			<a href="#" class="btn btn-sm btn-danger"><i class="icon-trash-3"></i> Limpar</a>
		</div>-->
		<div class="btn-group">
			<a href="javascript:LoadUrl(\'/App/notificacaoLis.php?id='.$nid.'\');" class="btn btn-sm btn-success">Ver todos <i class="icon-right-open-2"></i></a>
		</div>
	</div>
</li>';

$html .= str_repeat(\AppClass\App\ZWS::TAB,6).'</ul>'.\AppClass\App\ZWS::NL;
echo $html;