<?php
#################################################################################
## Includes
#################################################################################
if (defined('DOC_ROOT')) {
	include_once(DOC_ROOT . 'include.php');
}else{
	include_once('../include.php');
}

#################################################################################
## Resgata a variável ID que está criptografada
#################################################################################
if (isset($_GET['id'])) {
	$id = \AppClass\App\Util::antiInjection($_GET["id"]);
}elseif (isset($_POST['id'])) {
	$id = \AppClass\App\Util::antiInjection($_POST["id"]);
}elseif (isset($id)) 	{
	$id = \AppClass\App\Util::antiInjection($id);
}else{
	\AppClass\App\Erro::halt('Falta de Parâmetros');
}

#################################################################################
## Descompacta o ID
#################################################################################
\AppClass\App\Util::descompactaId($id);

#################################################################################
## Verifica se o usuário tem permissão no menu
#################################################################################
$system->checaPermissao($_codMenu_);

#################################################################################
## Resgata a url desse script
#################################################################################
$url		= ROOT_URL . '/App/'. basename(__FILE__);

#################################################################################
## Resgata os dados do grid
#################################################################################
try {
	$notificacoes	 = \AppClass\App\Notificacao::listarTodas();
} catch (\Exception $e) {
	\AppClass\App\Erro::halt($e->getMessage());
}
	
#################################################################################
## Cria o objeto do Grid (bootstrap)
#################################################################################
$grid			= \AppClass\App\Grid::criar(\AppClass\App\Grid\Tipo::TP_BOOTSTRAP, "");
$grid->adicionaTexto($tr->trans(''),	 			1, $grid::CENTER	,'');
$grid->adicionaTexto($tr->trans('USUARIO'),			5, $grid::CENTER	,'');
$grid->adicionaTexto($tr->trans('ASSUNTO'),	 		15, $grid::CENTER	,'ASSUNTO');
$grid->adicionaTexto($tr->trans('DATA'),	 		15, $grid::CENTER	,'');
$grid->adicionaTexto($tr->trans('STATUS'),	 		10, $grid::CENTER	,'');

$grid->adicionaIcone(null,'fa fa-search',$tr->trans('Ver'));
$grid->importaDadosDoctrine($notificacoes);

#################################################################################
## Popula os valores dos botões
#################################################################################
for ($i = 0; $i < sizeof($notificacoes); $i++) {
	$uid		= \AppClass\App\Util::encodeUrl('_codMenu_='.$_codMenu_.'&_icone_='.$_icone_.'&codNotifUsuario='.$notificacoes[$i]->CODIGO.'&url='.$url);
	
	if ( $notificacoes[$i]->COD_TIPO == "M" ){
		$tipo = '<i class="fa fa-envelope"></i>';
	}else if ( $notificacoes[$i]->COD_TIPO == "S" ){
		$tipo = '<i class="fa fa-heart"></i>';
	}else if ( $notificacoes[$i]->COD_TIPO == "A" ){
		$tipo = '<i class="fa fa-lightbulb-o"></i>';
	}
	
	$grid->setValorCelula($i,0, $tipo);
	$grid->setValorCelula($i,1, '#' . $notificacoes[$i]->COD_USUARIO_ENVIOU );	
	$grid->setValorCelula($i,3,\AppClass\App\Util::formatData($system->config["data"]["datetimeSimplesFormat"], $notificacoes[$i]->DATA_CRIADA));
	$grid->setValorCelula($i,4, $notificacoes[$i]->IND_LIDA == 1 ? '<span class="label label-info">Lida</span>' : '<span class="label label-warning">Não Lida</span>' );
	
	if( $notificacoes[$i]->COD_TIPO == "M" ){
		$grid->setUrlCelula($i,5,'javascript:AbreModal(\'/App/mensagemPer.php?id='.$uid.'\');"');
	}else {
		$grid->setUrlCelula($i,5,'javascript:AbreModal(\'/App/notificacaoLer.php?id='.$uid.'\');"');	
	}
}

#################################################################################
## Gerar o código html do grid
#################################################################################
try {
	$htmlGrid	= $grid->getHtmlCode();
} catch (\Exception $e) {
	\AppClass\App\Erro::halt($e->getMessage());
}

#################################################################################
## Carregando o template html
#################################################################################
$tpl	= new \AppClass\App\Template();
$tpl->load(HTML_PATH . 'templateLis.html');

#################################################################################
## Define os valores das variáveis
#################################################################################
$tpl->set('GRID'			,$htmlGrid);
$tpl->set('NOME'			,$tr->trans('Notificações'));
$tpl->set('IC'				,$_icone_);
$tpl->set('URLADD'		    ,"");

#################################################################################
## Por fim exibir a página HTML
#################################################################################
$tpl->show();
