<?php
################################################################################
# Includes
################################################################################
if (defined ( 'DOC_ROOT' )) {
	include_once (DOC_ROOT . 'include.php');
} else {
	include_once ('../include.php');
}

################################################################################
# Resgata a variável ID que está criptografada
################################################################################
if (isset ( $_GET ['id'] )) {
	$id = \AppClass\App\Util::antiInjection ( $_GET ["id"] );
} elseif (isset ( $_POST ['id'] )) {
	$id = \AppClass\App\Util::antiInjection ( $_POST ["id"] );
} elseif (isset ( $id )) {
	$id = \AppClass\App\Util::antiInjection ( $id );
} else {
	\AppClass\App\Erro::halt ( 'Falta de Parâmetros' );
}

################################################################################
# Descompacta o ID
################################################################################
\AppClass\App\Util::descompactaId ( $id );

################################################################################
# Verifica se o usuário tem permissão no menu
################################################################################
$system->checaPermissao ( $_codMenu_ );

$codUsuario = $system->getCodUsuario();

################################################################################
# Resgata as informações do banco
################################################################################
if ($codUsuario) {
	try {
		$info	 = $db->extraiPrimeiro('SELECT * FROM `SLSEG_PESSOA` P
				LEFT OUTER JOIN `SLSEG_USUARIO` U ON (U.COD_PESSOA = P.CODIGO) 
				WHERE U.CODIGO = :codigo', array(':codigo' => $codUsuario));
	} catch ( \Exception $e ) {
		\AppClass\App\Erro::halt ( $e->getMessage () );
	}
	
	$codTipo		 = ($info->COD_TIPO) ? $info->COD_TIPO : null;
	$nome			 = ($info->NOME) ? $info->NOME : null;
	$email			 = ($info->EMAIL) ? $info->EMAIL : null;
	$telefone		 = ($info->TELEFONE) ? $info->TELEFONE : null;
	$dtNascimento	 = ($info->DAT_NASCIMENTO) ? \AppClass\App\Util::formatData($system->config["data"]["dateFormat"], $info->DAT_NASCIMENTO) : null;
	$codSexo		 = ($info->COD_SEXO) ? $info->COD_SEXO : null;
	$tipoSangue		 = ($info->TIPO_SANGUINEO) ? $info->TIPO_SANGUINEO : null;
	
	$nomeUsu		 = \AppClass\App\Util::getNomeCompleto($nome);
	
	if ($codTipo == "DOA"){
		$perfilTipo = "Doador";
	} else if ($codTipo == "REC"){
		$perfilTipo = "Receptor";
	} else if ($codTipo == "PDO"){
		$perfilTipo = "Potencial Doador";
	} else {
		$perfilTipo = "ADMINISTRADOR";
	}
} else {
	$perfilTipo	 	 = null;
	$nome	  	 	 = null;
	$email			 = null;
	$telefone		 = null;
	$dtNascimento	 = null;
	$codSexo		 = null;
	$tipoSangue		 = null;
}

################################################################################
# Select de Sexo
################################################################################
try {
	$aSexo	 = $db->extraiTodos('SELECT * FROM `SLSEG_SEXO`', array());
	$oSexo = $system->geraHtmlCombo ( $aSexo, 'CODIGO', 'DESCRICAO', $codSexo, 'Selecione o sexo');
} catch ( \Exception $e ) {
	\AppClass\App\Erro::halt ( $e->getMessage (), __FILE__, __LINE__ );
}

################################################################################
# Select de Estado
################################################################################
try {
	$aTipoSangue	= $db->extraiTodos('SELECT * FROM `SLADM_TIPO_SANGUE`', array());
	$oTipoSangue 	= $system->geraHtmlCombo ( $aTipoSangue, 'CODIGO', 'DESCRICAO', $tipoSangue, 'Selecione o tipo sanguíneo');
} catch ( \Exception $e ) {
	\AppClass\App\Erro::halt ( $e->getMessage (), __FILE__, __LINE__ );
}

################################################################################
# Url Voltar
################################################################################
$urlVoltar = ROOT_URL . "/App/meusDadosLis.php?id=" . $id;

################################################################################
# Url Novo
################################################################################
$uid = \AppClass\App\Util::encodeUrl ( '_codMenu_=' . $_codMenu_ . '&_icone_=' . $_icone_ . '&codUsuario=' );
$urlNovo = ROOT_URL . "/App/meusDadosCad.php?id=" . $uid;

################################################################################
# Carregando o template html
################################################################################
$tpl = new \AppClass\App\Template ();
$tpl->load ( \AppClass\App\Util::getCaminhoCorrespondente ( __FILE__, \AppClass\App\ZWS::EXT_HTML ) );

################################################################################
# Define os valores das variáveis
################################################################################
$tpl->set ( 'URL_FORM'			   , $_SERVER ['SCRIPT_NAME'] );
$tpl->set ( 'URLVOLTAR'			   , $urlVoltar );
$tpl->set ( 'URLNOVO'		 	   , $urlNovo );
$tpl->set ( 'ID'				   , $id );
$tpl->set ( 'COD_USUARIO'		   , $codUsuario);
$tpl->set ( 'PERFIL_TIPO'		   , $perfilTipo);
$tpl->set ( 'NOME_PERFIL'		   , $nomeUsu["primeiro_nome"]." ".$nomeUsu["segundo_nome"]);
$tpl->set ( 'NOME'			 	   , $nome);
$tpl->set ( 'EMAIL'			 	   , $email);
$tpl->set ( 'TELEFONE'		 	   , $telefone);
$tpl->set ( 'DATA_NASCIMENTO'	   , $dtNascimento);
$tpl->set ( 'COD_SEXO'			   , $oSexo);
$tpl->set ( 'COD_TIPO_SANGUE'	   , $oTipoSangue);

$tpl->set ( 'DP', \AppClass\App\Util::getCaminhoCorrespondente ( __FILE__, \AppClass\App\ZWS::EXT_DP, \AppClass\App\ZWS::CAMINHO_RELATIVO ) );
################################################################################
# Por fim exibir a página HTML
################################################################################
$tpl->show ();

