<?php
################################################################################
# Includes
################################################################################
if (defined ( 'DOC_ROOT' )) {
	include_once (DOC_ROOT . 'include.php');
} else {
	include_once ('../include.php');
}

################################################################################
# Resgata a variável ID que está criptografada
################################################################################
if (isset ( $_GET ['id'] )) {
	$id = \AppClass\App\Util::antiInjection ( $_GET ["id"] );
} elseif (isset ( $_POST ['id'] )) {
	$id = \AppClass\App\Util::antiInjection ( $_POST ["id"] );
} elseif (isset ( $id )) {
	$id = \AppClass\App\Util::antiInjection ( $id );
} else {
	\AppClass\App\Erro::halt ( 'Falta de Parâmetros' );
}

################################################################################
# Descompacta o ID
################################################################################
\AppClass\App\Util::descompactaId ( $id );

################################################################################
# Verifica se o usuário tem permissão no menu
################################################################################
$system->checaPermissao ( $_codMenu_ );

################################################################################
# Resgata as informações do banco
################################################################################
if ($codNotifUsuario) {
	try {
		$info	 = $db->extraiPrimeiro('SELECT N.*, NU.IND_LIDA, NT.DESCRICAO AS TIPO FROM `SLNOT_NOTIFICACAO_USUARIO` AS NU
				LEFT OUTER JOIN `SLNOT_NOTIFICACAO` N ON (NU.COD_NOTIFICACAO = N.CODIGO)
				LEFT OUTER JOIN `SLNOT_NOTIFICACAO_TIPO` NT ON (N.COD_TIPO = NT.CODIGO)
					WHERE NU.CODIGO = :codigo', array(':codigo' => $codNotifUsuario));
	} catch ( \Exception $e ) {
		\AppClass\App\Erro::halt ( $e->getMessage () );
	}
	
	$tipo			 = ($info->TIPO) ? $info->TIPO : null;	
	$codSolicitacao	 = ($info->COD_SOLICITACAO) ? $info->COD_SOLICITACAO : null;
	$assunto		 = ($info->ASSUNTO) ? $info->ASSUNTO : null;
	$mensagem		 = ($info->MENSAGEM) ? $info->MENSAGEM : null;
	$nome			 = ($info->NOME) ? $info->NOME : null;
	$email			 = ($info->EMAIL) ? $info->EMAIL : null;
	$dataCriada		 = ($info->DATA_CRIADA) ? \AppClass\App\Util::formatData($system->config["data"]["dateFormat"], $info->DATA_CRIADA) : null;
	$indLida		 = ($info->IND_LIDA) ? $info->IND_LIDA : null;
	$hiddenSolic	 = "hidden";
	
	## Captura as info do tipo solicitacao
	if(isset($codSolicitacao) && !empty($codSolicitacao)){
		$info		 = null;
		try {
			$info	 = $db->extraiPrimeiro('SELECT S.CODIGO AS COD_SOLICITACAO, TS.DESCRICAO AS TIPO_SANGUE, S.COD_ESTADO, C.NOME AS CIDADE, S.COD_HEMOCENTRO, O.NOME AS HEMOCENTRO, S.RELATO, S.DATA_SOLICITACAO, S.DATA_PRAZO, SS.DESCRICAO AS STATUS
			 FROM `SLNOT_NOTIFICACAO_USUARIO` AS NU
				LEFT OUTER JOIN `SLNOT_NOTIFICACAO` N ON (NU.COD_NOTIFICACAO = N.CODIGO)
				LEFT OUTER JOIN `SLREC_SOLICITACAO` S ON (N.COD_SOLICITACAO = S.CODIGO)
				LEFT OUTER JOIN `SLADM_TIPO_SANGUE` TS ON (S.COD_TIPO_SANGUE = TS.CODIGO)
				LEFT OUTER JOIN `SLREC_SOLICITACAO_STATUS` SS ON (S.COD_STATUS = SS.CODIGO)
				LEFT OUTER JOIN `SLADM_ORGANIZACAO` O ON (S.COD_HEMOCENTRO = O.CODIGO)
				LEFT OUTER JOIN `SLADM_ESTADO` E ON (S.COD_ESTADO = E.COD_UF)
				LEFT OUTER JOIN `SLADM_CIDADE` C ON (S.COD_CIDADE = C.CODIGO)
					
					WHERE NU.CODIGO = :codigo', array(':codigo' => $codNotifUsuario));
		} catch ( \Exception $e ) {
			\AppClass\App\Erro::halt ( $e->getMessage () );
		}
		
		$tipoSangue	 		 = ($info->TIPO_SANGUE) ? $info->TIPO_SANGUE : null;
		$relato		 		 = ($info->RELATO) ? $info->RELATO : null;
		$dataSolicitacao	 = ($info->DATA_SOLICITACAO) ? $info->DATA_SOLICITACAO : null;
		$dataPrazo			 = ($info->DATA_PRAZO) ? $info->DATA_PRAZO : null;
		$codHemocentro  	 = ($info->COD_HEMOCENTRO) ? $info->COD_HEMOCENTRO : null;
		$hemocentro  		 = ($info->HEMOCENTRO) ? $info->HEMOCENTRO : null;
		$estado		  		 = ($info->COD_ESTADO) ? $info->COD_ESTADO : null;
		$cidade		  		 = ($info->CIDADE) ? $info->CIDADE : null;
		$status				 = ($info->STATUS) ? $info->STATUS : null;
		$hiddenSolic	 	 = "";
		$uid				 = \AppClass\App\Util::encodeUrl('_codMenu_='.$_codMenu_.'&_icone_='.$_icone_.'&codNotifUsuario='.$codNotifUsuario);
		$urlPerguntar		 = 'javascript:AbreModal(\'/App/mensagemPer.php?id='.$uid.'\');"';
		
		if($status == "SOLICITADO"){
			$status = '<span class="label label-warning">Solicitado</span>';
		}else if($status == "ACEITO"){
			$status = '<span class="label label-success">Aceito</span>';
		}
	}else{
		$tipoSangue		 = null;
		$relato			 = null;
		$dataSolicitacao = null;
		$dataPrazo		 = null;
		$codHemocentro   = null;
		$hemocentro      = null;
		$estado     	 = null;
		$cidade  	     = null;
		$status			 = null;
	}
	
	## Mudar status para lida
	if($indLida == 0)
		$oLer	= $db->Executa('UPDATE `SLNOT_NOTIFICACAO_USUARIO` SET `IND_LIDA`=:indLida,`DATA_LIDA`=:dataLida WHERE `CODIGO`=:codigo', 
 					array('codigo' => $codNotifUsuario, 'indLida' => 1, 'dataLida' => date('Y-m-d H:i:s') ));
} else {
	$codTipo	  	 = null;
	$codSolicitacao	 = null;
	$assunto		 = null;
	$mensagem		 = null;
	$nome			 = null;
	$email		 	 = null;
	$dataCriada		 = null;
	
	$tipoSangue		 = null;
	$relato			 = null;
	$dataSolicitacao = null;
	$dataPrazo		 = null;
	$codHemocentro   = null;
	$hemocentro      = null;
	$estado     	 = null;
	$cidade  	     = null;
	$status			 = null;
}

################################################################################
# Carregando o template html
################################################################################
$tpl = new \AppClass\App\Template ();
$tpl->load ( \AppClass\App\Util::getCaminhoCorrespondente ( __FILE__, \AppClass\App\ZWS::EXT_HTML ) );

################################################################################
# Define os valores das variáveis
################################################################################
$tpl->set ( 'URL_FORM'			   , $_SERVER ['SCRIPT_NAME'] );
$tpl->set ( 'ID'				   , $id );

$tpl->set ( 'TIPO'			   	   , $tipo);
$tpl->set ( 'COD_SOLICITACAO' 	   , $codSolicitacao);
$tpl->set ( 'ASSUNTO'		 	   , $assunto);
$tpl->set ( 'MENSAGEM'			   , $mensagem);
$tpl->set ( 'NOME'			   	   , $nome);
$tpl->set ( 'EMAIL'	   			   , $email);
$tpl->set ( 'COD_HEMOCENTRO'	   , $codHemocentro);
$tpl->set ( 'HEMOCENTRO'	   	   , $hemocentro);
$tpl->set ( 'ESTADO'		   	   , $estado);
$tpl->set ( 'CIDADE'		   	   , $cidade);
$tpl->set ( 'DATA_CRIADA'		   , \AppClass\App\Util::formatData($system->config["data"]["dateFormat"], $dataCriada));

## Tipo Solicitacao
$tpl->set ( 'URL_PERGUNTAR'		   , $urlPerguntar);
$tpl->set ( 'TIPO_SANGUE'		   , $tipoSangue);
$tpl->set ( 'RELATO'			   , $relato);
$tpl->set ( 'DATA_SOLICITACAO'	   , \AppClass\App\Util::formatData($system->config["data"]["dateFormat"], $dataSolicitacao));
$tpl->set ( 'DATA_PRAZO'	  	   , \AppClass\App\Util::formatData($system->config["data"]["dateFormat"], $dataPrazo));
$tpl->set ( 'STATUS'	   		   , $status);
$tpl->set ( 'HIDDEN_SOLIC'	   	   , $hiddenSolic);

$tpl->set ( 'DP', \AppClass\App\Util::getCaminhoCorrespondente ( __FILE__, \AppClass\App\ZWS::EXT_DP, \AppClass\App\ZWS::CAMINHO_RELATIVO ) );
################################################################################
# Por fim exibir a página HTML
################################################################################
$tpl->show ();

