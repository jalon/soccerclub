<?php
#################################################################################
## Includes
#################################################################################
if (defined('DOC_ROOT')) {
	include_once(DOC_ROOT . 'include.php');
}else{
 	include_once('../include.php');
}
 
#################################################################################
## Resgata os parâmetros passados pelo formulário
#################################################################################
if (isset($_POST['codUsuario']))		$codUsuario			= \AppClass\App\Util::antiInjection($_POST['codUsuario']);
if (isset($_POST['nome']))				$nome				= \AppClass\App\Util::antiInjection($_POST['nome']);
if (isset($_POST['email']))				$email				= \AppClass\App\Util::antiInjection($_POST['email']);
if (isset($_POST['telefone']))			$telefone			= \AppClass\App\Util::antiInjection($_POST['telefone']);
if (isset($_POST['dataNascimento']))	$dataNascimento		= \AppClass\App\Util::antiInjection($_POST['dataNascimento']);
if (isset($_POST['codSexo']))			$codSexo			= \AppClass\App\Util::antiInjection($_POST['codSexo']);
if (isset($_POST['codPerfil']))			$codPerfil			= \AppClass\App\Util::antiInjection($_POST['codPerfil']);

#################################################################################
## Limpar a variável de erro
#################################################################################
$err	= false;

#################################################################################
## Fazer validação dos campos
#################################################################################
/** Nome **/
if ((empty($nome))) {
	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans("Campo NOME é obrigatório"));
	$err	= 1;
}

/** Email **/
if ((empty($email))) {
	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans("Campo EMAIL é obrigatório"));
	$err	= 1;
}

/** Telefone **/
if ((empty($telefone))) {
	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans("Campo TELEFONE é obrigatório"));
	$err	= 1;
}

/** Data Nasc **/
if ((empty($dataNascimento))) {
	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans("Campo DATA NASCIMENTO é obrigatório"));
	$err	= 1;
}

/** Sexo **/
if ((empty($codSexo))) {
	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans("Campo SEXO é obrigatório"));
	$err	= 1;
}

$info	 = $db->extraiPrimeiro('SELECT * FROM `SLSEG_PESSOA` WHERE EMAIL = :email', array(':email' => $email));

if($info != null &&(!empty($info->CODIGO) != $codUsuario)){
	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans("Este EMAIL já foi cadastrado!"));
	$err	= 1;
}

if ($err != null) {
	echo '1'.\AppClass\App\Util::encodeUrl('||'.htmlentities($err));
 	exit;
}
 
#################################################################################
## Salvar no banco
#################################################################################
try {
	$db->transaction();
	
	$params = array();
	
	if (!empty($dataNascimento)) {
		$dtNasc		= $data = implode("-",array_reverse(explode("/",$dataNascimento)));
	}else{
		$dtNasc		= null;
	}
	
	$params += array(':nome' => $nome,
			':email' => $email,
			':telefone' => $telefone,
			':dataNascimento' => $dtNasc,
			':codSexo' => $codSexo
	);
	
	if ($codPerfil == 'ADM'){
		$senha		= \AppClass\App\Util::_geraSenha(8);
		$senhaCrypt = \AppClass\App\Crypt::crypt($email, $senha);
		$codStatus  = 1;
	} else {
		$senhaCrypt = null;
		$codStatus  = 0;
			
	}
	
	if (isset($codUsuario) && (!empty($codUsuario))) {
		$info	 	= $db->extraiPrimeiro('SELECT COD_PESSOA FROM `SLSEG_USUARIO` WHERE CODIGO = :codigo', array(':codigo' => $codUsuario));
		
		$params 	+= array(':codigo' => $info->COD_PESSOA);
		
 		$oPessoa	= $db->Executa('UPDATE `SLSEG_PESSOA` SET `NOME`=:nome,`EMAIL`=:email,`TELEFONE`=:telefone,`DAT_NASCIMENTO`=:dataNascimento,`COD_SEXO`=:codSexo WHERE `CODIGO`=:codigo', 
 				$params);
 		
 		/** ### Updade tipo do usuario ### **/
 		$paramUsu = array(':codigo' => $codUsuario,
 				':senha' => $senhaCrypt,
 				':codStatus' => $codStatus,
 				':codPerfil' => $codPerfil
 		);
 			
 		$oUsuario	= $db->Executa('UPDATE `SLSEG_USUARIO` SET `SENHA`=:senha, `COD_STATUS`=:codStatus, `COD_PERFIL`=:codPerfil WHERE `CODIGO`=:codigo',
 				$paramUsu);
 		
 	}else{
 		$oPessoa	= $db->Executa('INSERT INTO `SLSEG_PESSOA`(`NOME`, `EMAIL`, `TELEFONE`, `DAT_NASCIMENTO`, `COD_SEXO`) VALUES (:nome,:email,:telefone,:dataNascimento,:codSexo)', 
 				$params);
 		
 		/** ### Cricacao do usuario ### **/
 		$paramUsu = array(':codPessoa' => $oPessoa,
 				':usuario' => $email,
 				':senha' => $senhaCrypt,
 				':codStatus' => $codStatus,
 				':codPerfil' => $codPerfil
 		);
 		
 		$oUsuario	= $db->Executa('INSERT INTO `SLSEG_USUARIO`(`COD_PESSOA`, `USUARIO`, `SENHA`, `COD_STATUS`, `COD_PERFIL`) VALUES (:codPessoa,:usuario,:senha,:codStatus,:codPerfil)',
 				$paramUsu);
 	}
 	$db->commit();
 	
 	$oPessoa	= ($codUsuario) ? $codUsuario : $oPessoa;
 	
} catch (\Exception $e) {
	$db->rollBack();
	
 	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO, "Ocorreu um erro ao salvar o usuário. " . $e->getMessage());
 	echo '1'.\AppClass\App\Util::encodeUrl('||'.htmlentities($e->getMessage()));
 	exit;
}
 
$system->criaAviso(\AppClass\App\Aviso\Tipo::INFO,"Informações salvas com sucesso");
echo '0'.\AppClass\App\Util::encodeUrl('|'.$oPessoa);