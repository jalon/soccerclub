<?php
#################################################################################
## Includes
#################################################################################
if (defined('DOC_ROOT')) {
	include_once(DOC_ROOT . 'include.php');
}else{
	include_once('../include.php');
}

#################################################################################
## Resgata os parâmetros passados pelo formulario
#################################################################################
if (isset($_POST['codUsuario'])) 		$codUsuario		= \AppClass\App\Util::antiInjection($_POST['codUsuario']);

#################################################################################
## Limpar a variável de erro
#################################################################################
$err	= false;

#################################################################################
## Verificar se existe e excluir
#################################################################################
try {

	if (!isset($codUsuario) || (!$codUsuario)) {
		$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans('Parâmetro não informado'));
		die('1'.\AppClass\App\Util::encodeUrl('||'));
	}
	
	$info	 = $db->extraiPrimeiro('SELECT CODIGO, COD_PESSOA FROM `SLSEG_USUARIO`
					WHERE CODIGO = :codigo', array(':codigo' => $codUsuario));
	
	//$oUsuario = 	$em->getRepository ( 'Entidades\ZgrhuFuncionarioCargo' )->findOneBy(array ('codigo' => $codCargo));
	$oUsuario	 = $db->Executa('DELETE FROM `SLSEG_USUARIO` WHERE CODIGO = :codigo', array(':codigo' => $info->CODIGO));
	
	$oPessoa	 = $db->Executa('DELETE FROM `SLSEG_PESSOA` WHERE CODIGO = :codigo', array(':codigo' => $info->COD_PESSOA));
	
	/*if (!$oUsuario) {
		$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans('Usuário não encontrado'));
		die('1'.\AppClass\App\Util::encodeUrl('||'));
	}*/
	
} catch (\Exception $e) {
	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$e->getMessage());
	die('1'.\AppClass\App\Util::encodeUrl('||'));
	exit;
}

$system->criaAviso(\AppClass\App\Aviso\Tipo::INFO,$tr->trans("Usuário excluído com sucesso"));
echo '0'.\AppClass\App\Util::encodeUrl('|'.$oUsuario.'|');