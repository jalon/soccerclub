<?php
#################################################################################
## Includes
#################################################################################
if (defined('DOC_ROOT')) {
	include_once(DOC_ROOT . 'includeNoAuth.php');
}else{
 	include_once('../includeNoAuth.php');
}

use \Zend\Mail;
use \Zend\Mail\Message;
use \Zend\Mime\Message as MimeMessage;
use \Zend\Mime\Part as MimePart;
Use \Zend\Mime;

#################################################################################
## Variáveis globais
#################################################################################
global $em,$system,$tr;

#################################################################################
## Resgata os parâmetros passados pelo formulario
#################################################################################
if (isset($_POST['email'])) 			$email				= \AppClass\App\Util::antiInjection($_POST['email']);

#################################################################################
## Limpar a variável de erro
#################################################################################
$err	= false;

#################################################################################
## Fazer validação dos campos
#################################################################################
/** Email **/
if (!isset($email) || (empty($email))) {
	die ('1'.\AppClass\App\Util::encodeUrl('||'.htmlentities($tr->trans("Campo EMAIL é obrigatório !!!"))));
	$err	= 1;
}

$valEmail	= new \AppClass\App\Validador\Email();
if ($valEmail->isValid($email) == false){
	die ('1'.\AppClass\App\Util::encodeUrl('||'.htmlentities($tr->trans("EMAIL inválido !!!"))));
	$err	= 1;
}

$oEmail	= $em->getRepository('Entidades\ZgsegUsuario')->findOneBy(array('usuario' => $email));

if (($oEmail != null) && ($oEmail->getUsuario() == $email)){
	$codUsuario = $oEmail->getCodigo();
}else{
	die ('1'.\AppClass\App\Util::encodeUrl('||'.htmlentities($tr->trans("Email não existe!"))));
	$err 	= 1;
}

if ($err != null) {
	echo '1'.\AppClass\App\Util::encodeUrl('||'.htmlentities($err));
 	exit;
}


#################################################################################
## Salvar no banco
#################################################################################
try {
	
	$oRecSenha	= $em->getRepository('Entidades\ZgsegUsuarioRecSenha')->findOneBy(array('codUsuario' => $codUsuario, 'codStatus' => 'A'));
	if (!$oRecSenha){
		$oRecSenha	= new \Entidades\ZgsegUsuarioRecSenha();
	}else{
		$oStatusFec	= $em->getRepository('Entidades\ZgsegHistEmailStatus')->findOneBy(array('codigo' => 'C'));
		$oRecSenha->setCodStatus($oStatusFec);
		$oRecSenha->setDataAlteracao(new \DateTime("now"));
		$oRecSenha->setIpAlteracao(\AppClass\App\Util::getIPUsuario());
		$em->persist($oRecSenha);
		$em->flush();
		$em->detach($oRecSenha);
		$oRecSenha	= new \Entidades\ZgsegUsuarioRecSenha();
	}
	
 	if (!$oRecSenha) $oRecSenha	= new \Entidades\ZgsegUsuarioRecSenha();
 	
 	$oStatus	= $em->getRepository('Entidades\ZgsegHistEmailStatus')->findOneBy(array('codigo' => 'A'));
 	
 	$oRecSenha->setCodUsuario($oEmail);
 	$oRecSenha->setDataSolicitacao(new \DateTime("now"));
 	$oRecSenha->setIpSolicitacao(\AppClass\App\Util::getIPUsuario());
 	$oRecSenha->setSenhaAlteracao(\AppClass\Seg\Perfil::_geraSenha());
 	$oRecSenha->setCodStatus($oStatus);
 	
 	$em->persist($oRecSenha);
 	$em->flush();
 	$em->detach($oRecSenha);
 	
 	#################################################################################
 	## Gerar a notificação
 	#################################################################################
 	$cid 			= \AppClass\App\Util::encodeUrl('_cdu01='.$oRecSenha->getCodigo().'&_cdu02='.$oRecSenha->getSenhaAlteracao().'&_cdu03='.$oEmail->getUsuario().'&_cdu04='.$oRecSenha->getCodUsuario()->getCodigo().'&_cdu05='.$system->getCodOrganizacao());
 	$assunto		= "Redefinição da senha.";
 	$textoEmail		= "Solicitação de senha feita no site. Para prosseguir com a alteração clique no link abaixo:";
 	$nome			= $oRecSenha->getCodUsuario()->getNome();
 	$confirmUrl		= ROOT_URL . "/Seg/u04.php?cid=".$cid;
 	
 	$oRemetente		= $em->getReference('\Entidades\ZgsegUsuario',$codUsuario);
 	$template		= $em->getRepository('\Entidades\ZgappNotificacaoTemplate')->findOneBy(array('template' => 'USUARIO_CADASTRO'));
 	$notificacao	= new \AppClass\App\Notificacao(\AppClass\App\Notificacao::TIPO_MENSAGEM_TEMPLATE, \AppClass\App\Notificacao::TIPO_DEST_USUARIO);
 	$notificacao->setAssunto("Redefinição da senha.");
 	$notificacao->setCodRemetente($oRemetente);
 	
 	$notificacao->associaUsuario($oRecSenha->getCodUsuario()->getCodigo());
 	$notificacao->enviaEmail();
 	$notificacao->setCodTemplate($template);
 	$notificacao->adicionaVariavel("NOME"		, $nome);
 	$notificacao->adicionaVariavel("ASSUNTO"	, $assunto);
 	$notificacao->adicionaVariavel("TEXTO"		, $textoEmail);
 	$notificacao->adicionaVariavel("CONFIRM_URL", $confirmUrl);
 	
 	$notificacao->salva();
 	
 	$em->flush();
 	$em->clear();
	
} catch (\Exception $e) {
 	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$e->getMessage());
 	echo '1'.\AppClass\App\Util::encodeUrl('||'.htmlentities($e->getMessage()));
 	exit;
}
 
die ('0'.\AppClass\App\Util::encodeUrl('||'.htmlentities($tr->trans("Email enviado com sucesso!"))));
