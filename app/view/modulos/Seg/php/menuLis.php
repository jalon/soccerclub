<?php
#################################################################################
## Includes
#################################################################################
if (defined('DOC_ROOT')) {
	include_once(DOC_ROOT . 'include.php');
}else{
	include_once('../include.php');
}

#################################################################################
## Resgata a variável ID que está criptografada
#################################################################################
if (isset($_GET['id'])) {
	$id = \AppClass\App\Util::antiInjection($_GET["id"]);
}elseif (isset($_POST['id'])) {
	$id = \AppClass\App\Util::antiInjection($_POST["id"]);
}elseif (isset($id)) 	{
	$id = \AppClass\App\Util::antiInjection($id);
}else{
	\AppClass\App\Erro::halt('Falta de Parâmetros');
}

#################################################################################
## Descompacta o ID
#################################################################################
\AppClass\App\Util::descompactaId($id);

#################################################################################
## Verifica se o usuário tem permissão no menu
#################################################################################
$system->checaPermissao($_codMenu_);
/*
#################################################################################
## Resgata os parâmetros passados pelo formulario de pesquisa
#################################################################################
if (isset($_GET['busca'])) 			$busca			= \AppClass\App\Util::antiInjection($_GET['busca']);
if (isset($_GET['codModulo'])) 		$codModulo		= \AppClass\App\Util::antiInjection($_GET['codModulo']);

#################################################################################
## Resgata os parâmetros passados
#################################################################################
if (isset($_GET['codPasta'])) 		$codPastaSel	= \AppClass\App\Util::antiInjection($_GET['codPasta']);

#################################################################################
## Resgata os dados da árvore
#################################################################################
try {
	$modulos	= $db->extraiTodos('SELECT * FROM `SLAPP_MODULO`', array() );

	if (isset($codModulo) && (!empty($codModulo))) {
		$_SESSION["_segCodModulo"]	= $codModulo;
	}elseif (!isset($_SESSION["_segCodModulo"]) || (empty($_SESSION["_segCodModulo"]))) {
		$codModulo 					= $modulos[0]->CODIGO;
		$_SESSION["_segCodModulo"]	= $codModulo;
	}else{
		$codModulo		= $_SESSION["_segCodModulo"];
	}
	
	$arvore		= new \AppClass\App\Arvore();
	$arvore->exibirRaiz(true);
	$menus		= $db->extraiTodos('SELECT * FROM `SLAPP_MENU` WHERE COD_TIPO =:codTipo ORDER BY NIVEL ASC, CODIGO ASC', array(':codTipo' => "M") );
	
	for ($i = 0; $i < sizeof($menus); $i++) {
		$pastaMae	= ($menus[$i]->COD_PAI) ? $menus[$i]->COD_PAI : null;
		$arvore->adicionaPasta($menus[$i]->CODIGO, $menus[$i]->NOME, $pastaMae);
		$links		= $db->extraiTodos('SELECT * FROM `SLAPP_MENU` WHERE COD_TIPO =:codTipo AND COD_PAI =:codMenuPai ORDER BY NIVEL ASC, CODIGO ASC', array(':codTipo' => "L", ":codMenuPai" => $menus[$i]->CODIGO) );
		
		for ($j = 0; $j < sizeof($links); $j++) {
			$pastaMae	= ($links[$j]->COD_PAI) ? $links[$j]->COD_PAI : null;
			$arvore->adicionaItem($links[$j]->CODIGO, $links[$j]->NOME, $pastaMae);
		}
	}
	
	$links	= $db->extraiTodos('SELECT * FROM `SLAPP_MENU` WHERE COD_TIPO =:codTipo AND COD_PAI =:codMenuPai ORDER BY NIVEL ASC, CODIGO ASC', array(':codTipo' => "L", ":codMenuPai" => null) );
	for ($j = 0; $j < sizeof($links); $j++) {
		$pastaMae	= ($links[$j]->COD_PAI) ? $links[$j]->COD_PAI : null;
		$arvore->adicionaItem($links[$j]->CODIGO, $links[$j]->NOME, $pastaMae);
	}
	
	if (isset($busca) && (!empty($busca)) ) {
		$filtro		= $arvore->filtrar($busca);
	}
	
} catch(\Exception $e) {
	$result['status'] = 'ERR';
	$result['message'] = $e->getMessage();
	exit;
}

if (!isset($codPastaSel)) {
	$codPastaSel	= null;
}

#################################################################################
## Select dos módulos
#################################################################################
try {
	$oModulos	= $system->geraHtmlCombo($modulos, 'CODIGO', 'NOME', $codModulo, null);
} catch (\Exception $e) {
	\AppClass\App\Erro::halt($e->getMessage(),__FILE__,__LINE__);
}

if (isset($busca) && (!empty($busca))) {
	$filtro = '<button class="btn btn-white btn-info" onclick="buscaArvore();"><i class="ace-icon fa fa-times bigger-120 red"></i>Filtro: '.$busca.'</button>';
}else{
	$filtro	= null;
}

#################################################################################
## Resgata a url desse script
#################################################################################
$pid			= \AppClass\App\Util::encodeUrl('_codMenu_='.$_codMenu_.'&_icone_='.$_icone_.'&codModulo='.$codModulo);
$url			= ROOT_URL."/Seg/".basename(__FILE__)."?id=".$id;
$menuPerfilUrl	= ROOT_URL."/Seg/menuPerfilLis.php?id=".$pid;
*/

#################################################################################
## Select dos módulos
#################################################################################
if (isset($codModulo) && (!empty($codModulo))) {
	$_SESSION["_segCodModulo"]	= $codModulo;
}elseif (!isset($_SESSION["_segCodModulo"]) || (empty($_SESSION["_segCodModulo"]))) {
	$codModulo 					= $modulos[0]->CODIGO;
	$_SESSION["_segCodModulo"]	= $codModulo;
}else{
	$codModulo		= $_SESSION["_segCodModulo"];
}

$perfil		= $db->extraiTodos('SELECT * FROM `SLADM_PERFIL_ACESSO`', array() );
$grupo		= $db->extraiTodos('SELECT * FROM `SLADM_GRUPO_ACESSO`', array() );
$modulos	= $db->extraiTodos('SELECT * FROM `SLAPP_MODULO`', array() );

try {
	$oModulos	= $system->geraHtmlCombo($modulos, 'CODIGO', 'NOME', $codModulo, null);
} catch (\Exception $e) {
	\AppClass\App\Erro::halt($e->getMessage(),__FILE__,__LINE__);
}

try {
	$oPerfil	= $system->geraHtmlCombo($perfil, 'CODIGO', 'DESCRICAO', $codModulo, null);
} catch (\Exception $e) {
	\AppClass\App\Erro::halt($e->getMessage(),__FILE__,__LINE__);
}

try {
	$oGrupos	= $system->geraHtmlCombo($grupo, 'CODIGO', 'DESCRICAO', $system->getCodGrupoAcesso(), null);
} catch (\Exception $e) {
	\AppClass\App\Erro::halt($e->getMessage(),__FILE__,__LINE__);
}

#################################################################################
## Carregando o template html
#################################################################################
$tpl	= new \AppClass\App\Template();
$tpl->load(\AppClass\App\Util::getCaminhoCorrespondente(__FILE__, \AppClass\App\ZWS::EXT_HTML));

#################################################################################
## Define os valores das variáveis
#################################################################################
$tpl->set('ID'					,$id);
//$tpl->set('TREE_DATA'			,$arvore->getJsonCode());
//$tpl->set('TARGET'				,$system->getDivCentral());
//$tpl->set('URL'					,$url);
//$tpl->set('COD_PASTA_SEL'		,$codPastaSel);
//$tpl->set('COD_PASTA_RAIZ'		,\AppClass\App\Arvore::_codPastaRaiz);
$tpl->set('MODULOS'				,$oModulos);
$tpl->set('COD_MODULO'			,$codModulo);
$tpl->set('PERFIL_ACESSO'		,$oPerfil);
$tpl->set('GRUPO_ACESSO'		,$oGrupos);

//$tpl->set('FILTRO'				,$filtro);
//$tpl->set('MENU_PERFIL_URL'		,$menuPerfilUrl);

#################################################################################
## Por fim exibir a página HTML
#################################################################################
$tpl->show();

