<?php
#################################################################################
## Includes
#################################################################################
if (defined('DOC_ROOT')) {
	include_once(DOC_ROOT . 'includeNoAuth.php');
}else{
	include_once('includeNoAuth.php');
}

#################################################################################
## Resgata a variável CID que está criptografada
#################################################################################
if (isset($_GET['cid'])) $cid = \AppClass\App\Util::antiInjection($_GET["cid"]);
if (!isset($cid))		\AppClass\App\Erro::externalHalt('Página só pode ser usado por pessoas autorizadas');

#################################################################################
## Descompacta o ID
#################################################################################
\AppClass\App\Util::descompactaId($cid);

if (!isset($_key01))	\AppClass\App\Erro::externalHalt('Página só pode ser usado por pessoas autorizadas, COD_ERRO: 01');
if (!isset($_key02))	\AppClass\App\Erro::externalHalt('Página só pode ser usado por pessoas autorizadas, COD_ERRO: 02');
if (!isset($_key03))	\AppClass\App\Erro::externalHalt('Página só pode ser usado por pessoas autorizadas, COD_ERRO: 03');

#################################################################################
## Ajusta os nomes das variáveis
#################################################################################
$codPessoa		= $_key01;
$codUsuario		= $_key02;
$hashCode		= $_key03;

#################################################################################
## Verificar se os usuário já existe e se já está ativo
#################################################################################
$_oAuth	 = $db->extraiPrimeiro('SELECT U.* FROM `SLSEG_USUARIO` U
								LEFT OUTER JOIN `SLSEG_PESSOA` P ON (P.CODIGO = U.COD_PESSOA) 
									WHERE U.CODIGO =:codUsuario AND P.CODIGO =:codPessoa', array(':codUsuario' => $codUsuario, ':codPessoa' => $codPessoa ));

if (!$_oAuth || ( isset($_oAuth->scalar) && $_oAuth->scalar == false ) ) 	\AppClass\App\Erro::externalHalt('Usuario não existe, COD_ERRO: 04');
if ($_oAuth->COD_STATUS != 0)				\AppClass\App\Erro::externalHalt('Esta conta já está ativada, COD_ERRO: 05');
if ($_oAuth->HASH != $hashCode)				\AppClass\App\Erro::externalHalt('KEY de alteração não corresponde, COD_ERRO: 06');

################################################################################
# Carregando o template html
################################################################################
$tpl = new \AppClass\App\Template ();
$tpl->load ( \AppClass\App\Util::getCaminhoCorrespondente ( __FILE__, \AppClass\App\ZWS::EXT_HTML ) );

################################################################################
# Define os valores das variáveis
################################################################################
$tpl->set ( 'URL_FORM'			   , $_SERVER ['SCRIPT_NAME'] );
$tpl->set ( 'COD_USUARIO'		   , $_oAuth->CODIGO);
$tpl->set ( 'COD_PESSOA'		   , $_oAuth->COD_PESSOA);
$tpl->set ( 'COD_HASH'		  	   , $_oAuth->HASH);
$tpl->set ( 'EMAIL'				   , $_oAuth->USUARIO);

$tpl->set ( 'DP', \AppClass\App\Util::getCaminhoCorrespondente ( __FILE__, \AppClass\App\ZWS::EXT_DP, \AppClass\App\ZWS::CAMINHO_RELATIVO ) );
################################################################################
# Por fim exibir a página HTML
################################################################################
$tpl->show ();

