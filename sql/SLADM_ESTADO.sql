-- phpMyAdmin SQL Dump
-- version 4.2.13.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 20-Jan-2016 às 18:10
-- Versão do servidor: 5.6.17
-- PHP Version: 5.6.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `saveLife`
--

-- --------------------------------------------------------

--
-- Extraindo dados da tabela `SLADM_ESTADO`
--

INSERT INTO `SLADM_ESTADO` (`COD_UF`, `NOME`, `COD_REGIAO`) VALUES
('AC', 'ACRE', 'NO'),
('AL', 'ALAGOAS', 'NE'),
('AM', 'AMAZONAS', 'NO'),
('AP', 'AMAPÁ', 'NO'),
('BA', 'BAHIA', 'NE'),
('CE', 'CEARÁ', 'NE'),
('DF', 'DISTRITO FEDERAL', 'CE'),
('ES', 'ESPÍRITO SANTO', 'SE'),
('GO', 'GOIÁS', 'CE'),
('MA', 'MARANHÃO', 'NO'),
('MG', 'MINAS GERAIS', 'SU'),
('MS', 'MATO GROSSO DO SUL', 'CE'),
('MT', 'MATO GROSSO', 'CE'),
('PA', 'PARÁ', 'NO'),
('PB', 'PARAÍBA', 'NE'),
('PE', 'PERNANMBUCO', 'NE'),
('PI', 'PIAUÍ', 'NO'),
('PR', 'PARANÁ', 'SU'),
('RJ', 'RIO DE JANEIRO', 'SE'),
('RN', 'RIO GRANDE DO NORTE', 'NO'),
('RO', 'RONDÔNIA', 'NO'),
('RR', 'RORAIMA', 'NO'),
('RS', 'RIO GRANDE DO SUL', 'SU'),
('SC', 'SANTA CATARINA', 'SU'),
('SE', 'SERGIPE', 'NO'),
('SP', 'SÃO PAULO', 'SE'),
('TO', 'TOCANTINS', 'NO');

